﻿using UnityEngine;

namespace Sapphire.PostProcess
{
    [ExecuteInEditMode]
    [AddComponentMenu("Sapphire/PostProcess/Film Grain")]
    [RequireComponent(typeof(Camera))]
    public class FilmGrain : MonoBehaviour
    {
        [SerializeField]
        private float m_grainScale = 0.02f;
        public float GrainScale
        {
            get { return m_grainScale; }
            set { m_grainScale = value; }
        }

        [SerializeField]
        private float m_colorScale = 0.01f;
        public float ColorScale
        {
            get { return m_colorScale; }
            set { m_colorScale = value; }
        }

        [SerializeField]
        private float m_grainSize = 1.5f;
        public float GrainSize
        {
            get { return m_grainSize; }
            set { m_grainSize = value; }
        }

        [SerializeField]
        private float m_luminanceScale = 1.0f;
        public float LuminanceScale
        {
            get { return m_luminanceScale; }
            set { m_luminanceScale = value; }
        }

        [SerializeField]
        private bool m_colored = true;
        public bool Colored
        {
            get { return m_colored; }
            set { m_colored = value; }
        }


        [SerializeField]
        private Shader m_shader;

        private Material m_material;

        private bool m_isSupported = true;


        private void OnDisable()
        {
            if(m_material)
                DestroyImmediate(m_material);
        }


        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if(CheckResources() == false)
            {
                Graphics.Blit(source, destination);
                return;
            }

            ClampMaterialProperties();

            if(m_colored)
            {
                m_material.EnableKeyword("COLORED_GRAIN_ON");
            }
            else
            {
                m_material.DisableKeyword("COLORED_GRAIN_ON");
            }

            m_material.SetFloat("iGrainScale", m_grainScale);
            m_material.SetFloat("iColorScale", m_colorScale);
            m_material.SetFloat("iGrainSize", m_grainSize);
            m_material.SetFloat("iLuminanceScale", m_luminanceScale);

            Graphics.Blit(source, destination, m_material);
        }


        private void ClampMaterialProperties()
        {
            m_grainScale = Mathf.Clamp(m_grainScale, 0.0f, 0.1f);
            m_grainSize = Mathf.Clamp(m_grainSize, 0.25f, 3.0f);
            m_luminanceScale = Mathf.Clamp(m_luminanceScale, 0.0f, 2.0f);
            m_colorScale = Mathf.Clamp(m_colorScale, 0.0f,0.1f);
        }


        private bool CheckResources()
        {
            CheckSupport();

            m_material = CheckShaderAndCreateMaterial(m_shader, m_material);

            if(!m_isSupported)
                ReportAutoDisable();
            
            return m_isSupported;
        }


        private Material CheckShaderAndCreateMaterial(Shader s, Material m2Create)
        {
            if(!s)
            {
                Debug.Log("Missing shader in " + ToString());
                enabled = false;
                return null;
            }

            if(s.isSupported && m2Create && m2Create.shader == s)
                return m2Create;

            if(!s.isSupported)
            {
                NotSupported();
                Debug.Log("The shader " + s.ToString() + " on effect " + ToString() + " is not supported on this platform!");
                return null;
            }
            else
            {
                m2Create = new Material(s);
                m2Create.hideFlags = HideFlags.DontSave;
                if(m2Create)
                    return m2Create;
                else return null;
            }
        }


        private void Start()
        {
            CheckResources();
        }


        private void OnEnable()
        {
            m_isSupported = true;
        }


        private bool CheckSupport()
        {
            m_isSupported = true;

            if(!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures)
            {
                NotSupported();
                return false;
            }

            return true;
        }


        private void NotSupported()
        {
            enabled = false;
            m_isSupported = false;
            return;
        }


        private void ReportAutoDisable()
        {
            Debug.LogWarning("The image effect " + ToString() + " has been disabled as it's not supported on the current platform.");
        }
    }
}
