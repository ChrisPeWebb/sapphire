﻿Shader "Hidden/Sapphire/Ghosting" 
{
	Properties 
	{
		_MainTex ("Screen", 2D) = "" {}
	}
	
	CGINCLUDE

	#pragma multi_compile __ HALO_ON
	#pragma multi_compile __ GHOST_ON	
	#pragma multi_compile __ LENS_RAMP_ON
	#pragma multi_compile __ ANAMORPHIC_ON

	#pragma target 3.0

	#include "UnityCG.cginc"
	#include "SapphireCore.cginc" 

	sampler2D iLensColor;
	sampler2D iAnamorphicLensColor;

	uniform int iGhosts;
	uniform fixed iGhostDispersion;
	uniform fixed iGhostRadiusFade;
	uniform fixed iGhostIntensity;
	uniform fixed iHaloWidth;
	uniform fixed iHaloRadiusFade;
	uniform fixed iHaloIntensity;
	uniform fixed iChromaticDispersion;


	v2f_Sapphire_Core vert_Mirror( appdata_img v )
	{
		v2f_Sapphire_Core o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		
		// Invert UVs to flip the image
		o.uv =  -v.texcoord.xy + half2(1.0,1.0);

		#if UNITY_UV_STARTS_AT_TOP
		if (_MainTex_TexelSize.y < 0)
			o.uv.y = 1-o.uv.y;
		#endif	
		
		return o;
	}


	half3 chromaticallyDistortedTexture( in sampler2D tex, in half2 texcoord, in half2 direction, in half3 dispersion )
	{
		return half3( 
			tex2D(tex, texcoord + direction * dispersion.r).r,
			tex2D(tex, texcoord + direction * dispersion.g).g,
			tex2D(tex, texcoord + direction * dispersion.b).b
		);
	}




	//
	// Iterative aberration. Maybe move this to the final combination pass so that we dont need to compute it for each ghost?
	//
	fixed2 bDispersion(fixed2 coord, fixed amount)
	{
		fixed2 cm = coord - 0.5;
		const float dispersionRadius = 0.50;
		return coord + (coord - 0.5) * (pow(dot(cm, cm), dispersionRadius) * amount);
	}


	fixed linterp(fixed t)
	{
		return saturate(1.0 - abs(2.0 * t - 1.0));
	}


	fixed map(fixed t, fixed a, fixed b)
	{
		return saturate((t - a) / (b - a));
	}


	fixed3 spectralAberration(fixed t)
	{
		fixed3 aberration;

		fixed low = step(t, 0.5);
		fixed high = 1.0 - low;
		fixed w = linterp(map(t, 1.0 / 6.0, 5.0 / 6.0));

		aberration = fixed3(low, 1.0, high) * fixed3(1.0 - w, w, 1.0 - w);

		return pow(aberration, fixed3(1.0 / 2.2, 1.0 / 2.2, 1.0 / 2.2));
	}


	half3 chromaticallyDistortedTextureIterated(in sampler2D tex, in half2 texcoord, in half2 direction, in half3 dispersion)
	{
		const int iterations = 32;

		fixed2 coord = texcoord;
		fixed iterDelta = 1.0 / fixed(iterations);
		half3 accumulationColor = fixed3(0.0, 0.0, 0.0);
		fixed3 accumulationWeight = fixed3(0.0, 0.0, 0.0);

		SAPPHIRE_UNROLL_C(32)
		for (int i = 0; i < iterations; ++i)
		{
			fixed p = iterDelta * fixed(i);

			fixed3 weight = spectralAberration(p);

			accumulationWeight += weight;
			accumulationColor += weight * tex2D(tex, bDispersion(coord, -iChromaticDispersion * p)).rgb;
		}

		return accumulationColor;
	}

	//
	//
	//
	

	half4 frag(v2f_Sapphire_Core IN) : SV_Target
	{
		half2 ghostCenterVec = (half2(0.5,0.5) - IN.uv) * max(iGhostDispersion,0.00001);
		half2 direction = normalize(ghostCenterVec);
		half4 c = half4(0.0, 0.0, 0.0, 0.0);

		half3 dispersion = half3(-_MainTex_TexelSize.x * iChromaticDispersion, 0.0, _MainTex_TexelSize.x * iChromaticDispersion);

		#if defined(GHOST_ON)
			SAPPHIRE_UNROLL_C(8)
			for (int i = 0; i < iGhosts; ++i)
			{
				// Accumulate ghosts
				half2 offset = frac(IN.uv + ghostCenterVec * float(i));

				// We only want bright spots from the center of the frame, so we will weight samples
				float weight = length(half2(0.5, 0.5) - offset) / length(half2(0.5, 0.5));
				weight = pow(1.0 - weight, iGhostRadiusFade);

				#if defined(ANAMORPHIC_ON)
					#if defined(LENS_RAMP_ON)
						// Get the brightness of the ghost (length of the color) and tint it using the ramp
						c.rgb += length(tex2D(_MainTex, offset) * weight) * tex2D(iAnamorphicLensColor, half2((i*1.0) / (iGhosts*1.0), 0.0)) * iGhostIntensity;
					#else
						c.rgb += tex2D(_MainTex, offset) * weight * iGhostIntensity;
					#endif
				#else
					c.rgb += chromaticallyDistortedTexture(_MainTex, offset, direction, dispersion) * weight * iGhostIntensity;
				#endif
			}
			#if defined(LENS_RAMP_ON) && !defined(ANAMORPHIC_ON)
				// Modify accumulation via radially sampled 1D color lookup texture
				// TODO: Correct by aspect ratio to make the ramp a perfect circle. Also do this for halo
				c *= tex2D(iLensColor, length(half2(0.5, 0.5) - IN.uv) / length(half2(0.5, 0.5)));
			#endif
		#endif

		#if defined(HALO_ON)
			// Add a halo by radially sampling the source image
			half2 haloVec = normalize(ghostCenterVec) * iHaloWidth;
			half weight = length(half2(0.5, 0.5) - frac(IN.uv + haloVec)) / length(half2(0.5, 0.5));
			weight = pow(1.0 - weight, iHaloRadiusFade) * iHaloIntensity;

			c.rgb += chromaticallyDistortedTexture(_MainTex, IN.uv + haloVec, direction, dispersion) * weight;
		#endif

		#if defined(ANAMORPHIC_ON)
			// Sample the main tex, but un flip it ;)
			c += tex2D(_MainTex, -IN.uv.xy + half2(1.0, 1.0));
		#endif

		return c;
	}


	ENDCG 
	
	Subshader 
	{
		ZTest Always Cull Off ZWrite Off
	  	
			// Pass 0: Ghost/halo with settings based on keywords
		Pass 
		{    
			CGPROGRAM
			#pragma vertex vert_Mirror
			#pragma fragment frag
			ENDCG
		}

	}

	Fallback off
	
}
