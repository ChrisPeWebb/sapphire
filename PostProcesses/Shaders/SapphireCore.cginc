﻿#ifndef SAPPHIRE_CORE
#define SAPPHIRE_CORE

uniform sampler2D _MainTex;
half4 _MainTex_TexelSize;


struct v2f_Sapphire_Core
{
	float4 pos : SV_POSITION;
	float2 uv : TEXCOORD0;
};


v2f_Sapphire_Core vert_Sapphire_Core(appdata_img v)
{
	v2f_Sapphire_Core o;
	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	o.uv = v.texcoord.xy;

#if UNITY_UV_STARTS_AT_TOP
	if (_MainTex_TexelSize.y < 0)
		o.uv.y = 1 - o.uv.y;
#endif	

	return o;
}



#ifdef UNITY_COMPILER_HLSL
	#define SAPPHIRE_BRANCH		[branch]
	#define SAPPHIRE_FLATTEN	[flatten]
	#define SAPPHIRE_UNROLL		[unroll]
#if defined(SHADER_API_D3D9)
	#define SAPPHIRE_UNROLL_C(idx1)	[unroll(##idx1)]
#else
	#define SAPPHIRE_UNROLL_C(idx1)	
#endif
	#define SAPPHIRE_LOOP		[loop]
	#define SAPPHIRE_FASTOPT	[fastopt]
#else
	#define SAPPHIRE_BRANCH
	#define SAPPHIRE_FLATTEN
	#define SAPPHIRE_UNROLL
	#define SAPPHIRE_UNROLL_C(idx1)	
	#define SAPPHIRELOOP
	#define SAPPHIRE_FASTOPT
#endif


inline half4 remap4(float4 value, float4 low1, float4 high1, float4 low2, float4 high2)
{
	return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
}


inline half4 remap4(float4 value, float4 low, float4 high)
{
	return low + value * (high - low);
}


inline half4 remap(float value, float low1, float high1, float low2, float high2)
{
	return low2 + (value - low1) * (high2 - low2) / (high1 - low1);
}


inline half remap(float value, float low, float high)
{
	return low + value * (high - low);
}


#endif //!SAPPHIRE_CORE