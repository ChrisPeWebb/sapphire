﻿Shader "Hidden/Sapphire/ChromaticAberration" 
{
	Properties 
	{
		_MainTex( "Base (RGB)", 2D ) = "white" {}
	}

	CGINCLUDE

	#pragma exclude_renderers flash
	#pragma fragmentoption ARB_precision_hint_fastest 
	#pragma target 3.0

	#pragma multi_compile __ VIGNETTING_ON
	#pragma multi_compile __ FULL_ABERRATION_ON

	#include "UnityCG.cginc"
	#include "SapphireCore.cginc" 

	uniform fixed iDispersion;
	uniform fixed iDispersionRadius;
	uniform fixed iVignetteDarkness;
	uniform fixed iVignetteRadius;

	uniform int iIterations;


	fixed2 bDispersion(fixed2 coord, fixed amount)
	{
		fixed2 cm = coord - 0.5;
		return coord + (coord - 0.5) * (pow( dot(cm, cm), iDispersionRadius) * amount);
	}


	fixed linterp(fixed t )
	{
		return saturate( 1.0 - abs( 2.0 * t - 1.0 ) );
	}


	fixed map(fixed t, fixed a, fixed b )
	{
		return saturate( ( t - a ) / ( b - a ) );
	}


	fixed3 spectralAberration(fixed t )
	{
		fixed3 aberration;

		fixed low = step( t, 0.5 );
		fixed high = 1.0 - low;
		fixed w = linterp( map( t, 1.0 / 6.0, 5.0 / 6.0 ) );

		aberration = fixed3( low, 1.0, high ) * fixed3( 1.0 - w, w, 1.0 - w);

		return pow( aberration, fixed3( 1.0 / 2.2, 1.0 / 2.2, 1.0 / 2.2 ) );
	}


	fixed3 spectralAberrationAchromaticDoublet(fixed t)
	{
		fixed3 aberration = fixed3(0.0,0.0,0.0);

		fixed low = step(t, 0.5);
		fixed w = linterp(map(t, 1.0 / 6.0, 5.0 / 6.0));

		aberration = fixed3(1.0, low, 1.0) * fixed3(1.0 - w, w, 1.0 - w);

		return pow(aberration, fixed3(1.0 / 2.2, 1.0 / 2.2, 1.0 / 2.2));
	}


	half4 frag( v2f_Sapphire_Core i ) : SV_Target
	{
		fixed2 coord = i.uv;
		fixed iterDelta = 1.0 / fixed(iIterations);
		half3 accumulationColor = fixed3( 0.0, 0.0, 0.0 );
		fixed3 accumulationWeight = fixed3( 0.0, 0.0, 0.0 );

		SAPPHIRE_UNROLL_C(32)
		for (int i = 0; i < iIterations; ++i)
		{
			fixed p = iterDelta * fixed(i);

#if defined(FULL_ABERRATION_ON)
			fixed3 weight = spectralAberration( p );
#else
			fixed3 weight = spectralAberrationAchromaticDoublet(p);
#endif

			accumulationWeight += weight;
			accumulationColor += weight * tex2D( _MainTex, bDispersion( coord, -iDispersion * p ) ).rgb;
		}

#if defined(VIGNETTING_ON)
		fixed2 vCoord = coord - 0.5;
		fixed vignette = saturate(pow(1 - dot(vCoord, vCoord), iVignetteRadius) + iVignetteDarkness);

		return half4(accumulationColor.rgb / accumulationWeight, 1.0) * vignette;
#else
		return half4(accumulationColor.rgb / accumulationWeight, 1.0);
#endif
	}


	ENDCG

	Subshader
	{
		ZTest Always Cull Off ZWrite Off

		// Pass 0: Chromatic aberration with full/doublet and vignette based on keywords
		Pass
		{
			CGPROGRAM
			#pragma vertex vert_Sapphire_Core
			#pragma fragment frag
			ENDCG
		}
	}

	FallBack off
}
