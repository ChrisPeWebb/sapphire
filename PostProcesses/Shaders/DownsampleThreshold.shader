﻿Shader "Hidden/Sapphire/DownsampleThreshold" 
{
	Properties 
	{
		_MainTex ("Screen", 2D) = "" {}
	}
	
	CGINCLUDE

	#include "UnityCG.cginc"

	#pragma multi_compile THRESHOLD_LUMINANCE THRESHOLD_INTENSITY

	#if defined(THRESHOLD_LUMINANCE)
	#define THRESHOLD_DOT half3(0.212671, 0.71516, 0.072169)
	#else
	#define THRESHOLD_DOT half3(0.3, 0.3, 0.3)
	#endif

	
	struct v2f 
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f_mt 
	{
		float4 pos : SV_POSITION;
		float2 uv[4] : TEXCOORD0;
	};
			
	sampler2D _MainTex;
	
	half4 _MainTex_TexelSize;

	uniform half iBias;
	uniform half iMax;
	uniform half iScale;
	uniform half4 iTint;

	v2f vert( appdata_img v ) 
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv =  v.texcoord.xy;
		
		#if UNITY_UV_STARTS_AT_TOP
		if (_MainTex_TexelSize.y < 0)
			o.uv.y = 1-o.uv.y;
		#endif	
		
		return o;
	}

	v2f_mt vertMultiTap( appdata_img v ) 
	{
		v2f_mt o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv[0] = v.texcoord.xy + _MainTex_TexelSize.xy * 0.5;
		o.uv[1] = v.texcoord.xy - _MainTex_TexelSize.xy * 0.5;	
		o.uv[2] = v.texcoord.xy - _MainTex_TexelSize.xy * half2(1,-1) * 0.5;	
		o.uv[3] = v.texcoord.xy + _MainTex_TexelSize.xy * half2(1,-1) * 0.5;	
		return o;
	}


	half4 Threshold_NoMax(half4 originalColor)
	{
		half3 c = max(half3(0.0, 0.0, 0.0), originalColor.rgb - half3(iBias, iBias, iBias));
		return originalColor * dot(c, THRESHOLD_DOT) * iScale;
	}


	half4 Threshold(half4 originalColor)
	{
		originalColor.rgb = min(half3(iMax, iMax, iMax), originalColor.rgb);

		half3 c = max(half3(0.0, 0.0, 0.0), originalColor.rgb - half3(iBias, iBias, iBias));

		return originalColor * dot(c, THRESHOLD_DOT) * iScale;
	}
	

	half4 fragThreshold(v2f i) : SV_Target 
	{
		return Threshold(tex2D(_MainTex, i.uv));
	}


	half4 fragThresholdMultiTap(v2f_mt i) : SV_Target 
	{
		half4 c = tex2D(_MainTex, i.uv[0].xy);

		c += tex2D(_MainTex, i.uv[1].xy);
		c += tex2D(_MainTex, i.uv[2].xy);
		c += tex2D(_MainTex, i.uv[3].xy);
		c *= 0.25;

		return Threshold(c);
	}


	half4 fragThresholdTint(v2f i) : SV_Target
	{
		return Threshold(tex2D(_MainTex, i.uv)) * iTint;
	}


	half4 fragThresholdMultiTapTint(v2f_mt i) : SV_Target
	{
		half4 c = tex2D(_MainTex, i.uv[0].xy);

		c += tex2D(_MainTex, i.uv[1].xy);
		c += tex2D(_MainTex, i.uv[2].xy);
		c += tex2D(_MainTex, i.uv[3].xy);
		c *= 0.25;

		return Threshold(c) * iTint;
		//return length(max(half4(0.0, 0.0, 0.0, 0.0), c - half4(iBias, iBias, iBias, iBias)) * iScale) * iTint;
		//return max(half4(0.0, 0.0, 0.0, 0.0), c - half4(iBias, iBias, iBias, iBias)) * iScale * iTint;
		//c.rgb += iTint.rgb;
		//return max(half4(0.0, 0.0, 0.0, 0.0), c - half4(iBias, iBias, iBias, iBias)) * iScale;
	}


	ENDCG 
	
	Subshader 
	{
		ZTest Always Cull Off ZWrite Off
	  	
		// Pass 0: Regular downsample with threshold
		Pass 
		{    
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragThreshold
			ENDCG
		}

		// Pass 1: Stable downsample with threshold
		Pass 
		{    
			CGPROGRAM
			#pragma vertex vertMultiTap
			#pragma fragment fragThresholdMultiTap
			ENDCG
		}

		// Pass 2: Regular downsample with threshold and tint
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragThresholdTint
			ENDCG
		}

		// Pass 3: Stable downsample with threshold and tint
		Pass
		{
			CGPROGRAM
			#pragma vertex vertMultiTap
			#pragma fragment fragThresholdMultiTapTint
			ENDCG
		}
	}

	Fallback off
	
}
