﻿Shader "Hidden/Sapphire/FlareBlend"
{
	Properties
	{
		_MainTex("Screen", 2D) = "" {}
	}

	CGINCLUDE

	#pragma multi_compile BLENDING_ADDITIVE BLENDING_NONE
	#pragma multi_compile __ FLARE_ON
	#pragma multi_compile __ HALO_ON
	#pragma multi_compile __ OVERLAY_ON
	#pragma multi_compile __ HALO_DIFFRACTION_ON
	#pragma multi_compile __ SEPERATED_HALO_ON
	#pragma multi_compile __ REMAP_OVERLAY_ON

	#include "UnityCG.cginc"
	#include "SapphireCore.cginc" 

	uniform half iOverlayMin;
	uniform half iOverlayMax;

	uniform sampler2D iFlareTex;
	uniform sampler2D iSeperatedHaloTex;
	uniform sampler2D iLensOverlayTex;
	uniform sampler2D iHaloDiffractionOverlay;


	half4 frag(v2f_Sapphire_Core i) : SV_Target
	{
		half4 c = half4(0.0,0.0,0.0,0.0);

		// Add flare contribution. This also include ghosts, if they were done
		#if defined(FLARE_ON)
			// If modulate flare by overlay if we need to
			#if defined(OVERLAY_ON)
				//Remap the overlay if we should
				#if defined(REMAP_OVERLAY_ON)
					half4 overlay = remap4(tex2D(iLensOverlayTex, i.uv),
						half4(iOverlayMin, iOverlayMin, iOverlayMin, 1.0),
						half4(iOverlayMax, iOverlayMax, iOverlayMax, 1.0));
				#else
					half4 overlay = tex2D(iLensOverlayTex, i.uv);
				#endif// OVERLAY_ON

				// Modulate by overlay AND diffraction if we are set up to do both
				// If we are doing seperated halo, dont do diffraction here. That will be done when halo is applied seperatly 
				#if defined(HALO_DIFFRACTION_ON) && !defined(SEPERATED_HALO_ON)
					c += tex2D(iFlareTex, i.uv) * (overlay + tex2D(iHaloDiffractionOverlay, i.uv));
				#else 
					c += tex2D(iFlareTex, i.uv) * overlay;
				#endif // HALO_DIFFRACTION_ON
			#else 
				c += tex2D(iFlareTex, i.uv);
			#endif // OVERLAY_ON
		#endif // FLARE_ON

		#if defined(SEPERATED_HALO_ON)
			#if defined(HALO_DIFFRACTION_ON)
				c += tex2D(iSeperatedHaloTex, i.uv) * tex2D(iHaloDiffractionOverlay, i.uv);
			#else 
				c += tex2D(iSeperatedHaloTex, i.uv);
			#endif // HALO_DIFFRACTION_ON
		#endif // SEPERATED_HALO_ON

		#if defined(BLENDING_ADDITIVE)
			// Add current frame to flare
			c += tex2D(_MainTex, i.uv);
		#endif // BLENDING_ADDITIVE

		return c;
	}


	ENDCG

	Subshader
	{
		ZTest Always Cull Off ZWrite Off

		// Pass 0: Flare blend with settings based on keywords
		Pass
		{
			CGPROGRAM
			#pragma vertex vert_Sapphire_Core
			#pragma fragment frag
			ENDCG
		}
	}

	Fallback off

}
