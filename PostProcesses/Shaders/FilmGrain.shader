﻿Shader "Hidden/Sapphire/Film Grain"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}

		Subshader
	{
		Pass
		{
			ZTest Always Cull Off ZWrite Off

			CGPROGRAM

#define PERMUTATION_TEXEL_SIZE 128.0
#define HALF_PERMUTATION_TEXEL_SIZE 64.0
#define LUMINANCE_COEFFICIENT half3(0.299, 0.587, 0.114)
#define COORD_ROTATION_OFFSET half3(1.425, 3.892, 5.835)

#pragma multi_compile __ COLORED_GRAIN_ON

#pragma vertex vert
#pragma fragment frag

#pragma exclude_renderers flash
#pragma fragmentoption ARB_precision_hint_fastest 
#pragma target 3.0

#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float4 _MainTex_TexelSize;

			uniform fixed iGrainScale;
			uniform fixed iColorScale;
			uniform fixed iGrainSize;
			uniform fixed iLuminanceScale;


			// Pretty consistent hash function. Might work nicer than noise3D on more hardware.
			half3 hash32(half2 p)
			{
				half3 p3 = frac(half3(p.xyx) * half3(443.8975, 397.2973, 491.1871));
				p3 += dot(p3.zxy, p3.yxz + 19.19);
				return frac(half3(p3.x * p3.y, p3.x * p3.z, p3.y * p3.z));
			}


			// Consider using a pre-computed perturbation texture. be kind to the old alu
			half4 random(in half2 coord)
			{
				half noiseBase = sin(dot(coord + half2(_Time.y, _Time.y), half2(12.9898, 78.233))) * 43758.5453;

				half noiseX = frac(noiseBase) * 2.0 - 1.0;
				half noiseY = frac(noiseBase * 1.2154) * 2.0 - 1.0;
				half noiseZ = frac(noiseBase * 1.3453) * 2.0 - 1.0;
				half noiseW = frac(noiseBase * 1.3647) * 2.0 - 1.0;

				return half4(noiseX, noiseY, noiseZ, noiseW);
			}


			half fade(in half t)
			{
				return t * t * t * (t * (t * 6.0 - 15.0) + 10.0);
			}


			half noise3D(in half3 position)
			{
				// Split position into int and fraction part.  Offset int part by half texel to sample centers
				half3 positionInteger = PERMUTATION_TEXEL_SIZE * floor(position) + HALF_PERMUTATION_TEXEL_SIZE;
				half3 positionFractional = frac(position);

				half perm00 = random(positionInteger.xy).a;
				half3  grad000 = random(half2(perm00, positionInteger.z)).rgb * 4.0 - 1.0;
				half n000 = dot(grad000, positionFractional);
				half3  grad001 = random(half2(perm00, positionInteger.z + PERMUTATION_TEXEL_SIZE)).rgb * 4.0 - 1.0;
				half n001 = dot(grad001, positionFractional - half3(0.0, 0.0, 1.0));

				half perm01 = random(positionInteger.xy + half2(0.0, PERMUTATION_TEXEL_SIZE)).a;
				half3  grad010 = random(half2(perm01, positionInteger.z)).rgb * 4.0 - 1.0;
				half n010 = dot(grad010, positionFractional - half3(0.0, 1.0, 0.0));
				half3  grad011 = random(half2(perm01, positionInteger.z + PERMUTATION_TEXEL_SIZE)).rgb * 4.0 - 1.0;
				half n011 = dot(grad011, positionFractional - half3(0.0, 1.0, 1.0));

				half perm10 = random(positionInteger.xy + half2(PERMUTATION_TEXEL_SIZE, 0.0)).a;
				half3  grad100 = random(half2(perm10, positionInteger.z)).rgb * 4.0 - 1.0;
				half n100 = dot(grad100, positionFractional - half3(1.0, 0.0, 0.0));
				half3  grad101 = random(half2(perm10, positionInteger.z + PERMUTATION_TEXEL_SIZE)).rgb * 4.0 - 1.0;
				half n101 = dot(grad101, positionFractional - half3(1.0, 0.0, 1.0));

				half perm11 = random(positionInteger.xy + half2(PERMUTATION_TEXEL_SIZE, PERMUTATION_TEXEL_SIZE)).a;
				half3  grad110 = random(half2(perm11, positionInteger.z)).rgb * 4.0 - 1.0;
				half n110 = dot(grad110, positionFractional - half3(1.0, 1.0, 0.0));
				half3  grad111 = random(half2(perm11, positionInteger.z + PERMUTATION_TEXEL_SIZE)).rgb * 4.0 - 1.0;
				half n111 = dot(grad111, positionFractional - half3(1.0, 1.0, 1.0));

				half4 blendX = lerp(half4(n000, n001, n010, n011), half4(n100, n101, n110, n111), fade(positionFractional.x));

				half2 blendXY = lerp(blendX.xy, blendX.zw, fade(positionFractional.y));

				half blendXYZ = lerp(blendXY.x, blendXY.y, fade(positionFractional.z));

				return blendXYZ;
			}


			half2 rotate2D(in half2 position, in half angle)
			{
				half aspect = _ScreenParams.x / _ScreenParams.y;

				half rotX = ((position.x * 2.0 - 1.0) * aspect * cos(angle)) - ((position.y * 2.0 - 1.0) * sin(angle));
				half rotY = ((position.y * 2.0 - 1.0) * cos(angle)) + ((position.x * 2.0 - 1.0) * aspect * sin(angle));

				rotX = ((rotX / aspect) * 0.5 + 0.5);
				rotY = rotY * 0.5 + 0.5;

				return half2(rotX, rotY);
			}


			struct v2f
			{
				float4 pos : SV_POSITION;
				fixed2 uv : TEXCOORD0;
			};


			v2f vert(appdata_img v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.texcoord;

#if UNITY_UV_STARTS_AT_TOP
				if (_MainTex_TexelSize.y < 0)
					o.uv.y = 1 - o.uv.y;
#endif	

				return o;
			}


			half4 frag(v2f i) : SV_Target
			{
				half2 coord = i.uv;

				half2 rotatedCoord = rotate2D(coord, _Time.y + COORD_ROTATION_OFFSET.x);

				half noise = noise3D(half3(rotatedCoord * half2(_ScreenParams.x / iGrainSize, _ScreenParams.y / iGrainSize), 0.0));

#ifdef COLORED_GRAIN_ON

				half3 noise3;

				noise3.r = noise;

				half2 rotatedCoordG = rotate2D(coord, _Time.y + COORD_ROTATION_OFFSET.y);
				half2 rotatedCoordB = rotate2D(coord, _Time.y + COORD_ROTATION_OFFSET.z);

				noise3.g = lerp(noise.r, noise3D(half3(rotatedCoordG * half2(_ScreenParams.x / iGrainSize, _ScreenParams.y / iGrainSize), 1.0)), iColorScale);
				noise3.b = lerp(noise.r, noise3D(half3(rotatedCoordB * half2(_ScreenParams.x / iGrainSize, _ScreenParams.y / iGrainSize), 2.0)), iColorScale);

#else

				half3 noise3 = half3(noise, noise, noise);

#endif

				half3 col = tex2D(_MainTex, coord);

				float luminance = lerp(0.0, dot(col, LUMINANCE_COEFFICIENT), iLuminanceScale);
				noise3 = lerp(noise3, half3(0.0, 0.0, 0.0), pow(smoothstep(0.2, 0.0, luminance) + luminance, 4.0));

				col = col + noise3 * iGrainScale;

				return half4(col, 1.0);
			}

				ENDCG
		}
	}

	FallBack off
}
