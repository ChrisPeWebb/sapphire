﻿Shader "Hidden/Sapphire/Blur"
{
	Properties
	{
		_MainTex("Screen", 2D) = "" {}
	}

	CGINCLUDE

	#include "UnityCG.cginc"

	static const half curve[7] = { 0.0205, 0.0855, 0.232, 0.324, 0.232, 0.0855, 0.0205 };

	static const half4 curve4[7] = { half4(0.0205,0.0205,0.0205,0), half4(0.0855,0.0855,0.0855,0), half4(0.232,0.232,0.232,0),
		half4(0.324,0.324,0.324,1), half4(0.232,0.232,0.232,0), half4(0.0855,0.0855,0.0855,0), half4(0.0205,0.0205,0.0205,0) };


	struct v2f
	{
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f_SGX
	{
		float4 pos : SV_POSITION;
		half2 uv : TEXCOORD0;
		half4 offs[3] : TEXCOORD1;
	};

	struct v2f_Mobile_Gaussian
	{
		float4 pos : SV_POSITION;
		half4 uv : TEXCOORD0;
		half2 offs : TEXCOORD1;
	};

	struct v2f_Sep_Gaussian 
	{
		half4 pos : SV_POSITION;
		half2 uv : TEXCOORD0;
		half4 uv01 : TEXCOORD1;
		half4 uv23 : TEXCOORD2;
		half4 uv45 : TEXCOORD3;
		half4 uv67 : TEXCOORD4;
	};


	sampler2D _MainTex;
	uniform half4 _MainTex_TexelSize;

	uniform half4 iOffsets;


	v2f vert(appdata_img v)
	{
		v2f o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv = v.texcoord.xy;

		return o;
	}

	v2f_SGX vertSGXHorizontal(appdata_img v)
	{
		v2f_SGX o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		o.uv = v.texcoord.xy;
		half2 netFilterWidth = _MainTex_TexelSize.xy * half2(1.0, 0.0) * iOffsets.x;
		half4 coords = -netFilterWidth.xyxy * 3.0;

		o.offs[0] = v.texcoord.xyxy + coords * half4(1.0h, 1.0h, -1.0h, -1.0h);
		coords += netFilterWidth.xyxy;
		o.offs[1] = v.texcoord.xyxy + coords * half4(1.0h, 1.0h, -1.0h, -1.0h);
		coords += netFilterWidth.xyxy;
		o.offs[2] = v.texcoord.xyxy + coords * half4(1.0h, 1.0h, -1.0h, -1.0h);

		return o;
	}

	v2f_SGX vertSGXVertical(appdata_img v)
	{
		v2f_SGX o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		o.uv = half4(v.texcoord.xy, 1, 1);
		half2 netFilterWidth = _MainTex_TexelSize.xy * half2(0.0, 1.0) * iOffsets.x;
		half4 coords = -netFilterWidth.xyxy * 3.0;

		o.offs[0] = v.texcoord.xyxy + coords * half4(1.0h, 1.0h, -1.0h, -1.0h);
		coords += netFilterWidth.xyxy;
		o.offs[1] = v.texcoord.xyxy + coords * half4(1.0h, 1.0h, -1.0h, -1.0h);
		coords += netFilterWidth.xyxy;
		o.offs[2] = v.texcoord.xyxy + coords * half4(1.0h, 1.0h, -1.0h, -1.0h);

		return o;
	}


	v2f_Mobile_Gaussian vertMobileGaussianHorizontal(appdata_img v)
	{
		v2f_Mobile_Gaussian o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		o.uv = half4(v.texcoord.xy, 1, 1);
		o.offs = _MainTex_TexelSize.xy * half2(1.0, 0.0) * iOffsets.x;

		return o;
	}


	v2f_Mobile_Gaussian vertMobileGaussianVertical(appdata_img v)
	{
		v2f_Mobile_Gaussian o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		o.uv = half4(v.texcoord.xy, 1, 1);
		o.offs = _MainTex_TexelSize.xy * half2(0.0, 1.0) * iOffsets.x;

		return o;
	}


	v2f_Sep_Gaussian vertSepGaussian(appdata_img v)
	{
		v2f_Sep_Gaussian  o;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

		o.uv.xy = v.texcoord.xy;

		o.uv01 = v.texcoord.xyxy + iOffsets.xyxy * half4(1, 1, -1, -1);
		o.uv23 = v.texcoord.xyxy + iOffsets.xyxy * half4(1, 1, -1, -1) * 2.0;
		o.uv45 = v.texcoord.xyxy + iOffsets.xyxy * half4(1, 1, -1, -1) * 3.0;
		o.uv67 = v.texcoord.xyxy + iOffsets.xyxy * half4(1, 1, -1, -1) * 6.5;

		return o;
	}


	half4 frag(v2f i) : SV_Target
	{
		return tex2D(_MainTex, i.uv);
	}


	half4 fragSGX(v2f_SGX i) : SV_Target
	{
		half2 uv = i.uv.xy;

		half4 color = tex2D(_MainTex, i.uv) * curve4[3];

		for (int l = 0; l < 3; l++)
		{
			half4 tapA = tex2D(_MainTex, i.offs[l].xy);
			half4 tapB = tex2D(_MainTex, i.offs[l].zw);
			color += (tapA + tapB) * curve4[l];
		}

		return color;
	}


	half4 fragMobileGaussian(v2f_Mobile_Gaussian i) : SV_Target
	{
		half2 uv = i.uv.xy;
		half2 netFilterWidth = i.offs;
		half2 coords = uv - netFilterWidth * 3.0;

		half4 color = 0;
		for (int l = 0; l < 7; l++)
		{
			half4 tap = tex2D(_MainTex, coords);
				color += tap * curve4[l];
			coords += netFilterWidth;
		}

		return color;
	}


	half4 fragSepGaussian(v2f_Sep_Gaussian  i) : SV_Target
	{
		half4 color = half4 (0,0,0,0);

		color += 0.225 * tex2D(_MainTex, i.uv);
		color += 0.150 * tex2D(_MainTex, i.uv01.xy);
		color += 0.150 * tex2D(_MainTex, i.uv01.zw);
		color += 0.110 * tex2D(_MainTex, i.uv23.xy);
		color += 0.110 * tex2D(_MainTex, i.uv23.zw);
		color += 0.075 * tex2D(_MainTex, i.uv45.xy);
		color += 0.075 * tex2D(_MainTex, i.uv45.zw);
		color += 0.0525 * tex2D(_MainTex, i.uv67.xy);
		color += 0.0525 * tex2D(_MainTex, i.uv67.zw);

		return color;
	}


	ENDCG

	Subshader
	{
		ZTest Always Cull Off ZWrite Off

		// Pass 0: Box - Vertical
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}

		// Pass 1: Box - Horizontal
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			ENDCG
		}

		// Pass 2: SGX - Vertical
		Pass
		{
			CGPROGRAM
			#pragma vertex vertSGXVertical
			#pragma fragment fragSGX
			ENDCG
		}

		// Pass 3: SGX - Horizontal
		Pass
		{
			CGPROGRAM
			#pragma vertex vertSGXHorizontal
			#pragma fragment fragSGX
			ENDCG
		}

		// Pass 4: Mobile - Vertical
		Pass
		{
			CGPROGRAM
			#pragma vertex vertMobileGaussianVertical
			#pragma fragment fragMobileGaussian
			ENDCG
		}

		// Pass 5: Mobile - Horizontal
		Pass
		{
			CGPROGRAM
			#pragma vertex vertMobileGaussianHorizontal
			#pragma fragment fragMobileGaussian
			ENDCG
		}

		// Pass 6: Seperable
		Pass
		{
			CGPROGRAM
			#pragma vertex vertSepGaussian
			#pragma fragment fragSepGaussian
			ENDCG
		}


	}

	Fallback off

}
