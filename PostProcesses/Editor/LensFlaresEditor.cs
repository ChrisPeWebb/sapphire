﻿using UnityEngine;
using UnityEditor;
using System.IO;

using Sapphire.PostProcess;

namespace Sapphire.Editors
{
    [CustomEditor(typeof(LensFlares))]
    public class LensFlaresEditor : Editor
    {
        private SerializedObject m_serializedObject;

        private SerializedProperty m_downsampleFactor;
        private SerializedProperty m_thresholdMode;
        private SerializedProperty m_thresholdBias;
        private SerializedProperty m_thresholdMax;
        private SerializedProperty m_thresholdScale;
        private SerializedProperty m_visualizeThreshold;

        private SerializedProperty m_flareMode;
        private SerializedProperty m_anamorphicFlareDirection;
        private SerializedProperty m_smoothAnamorphicFlare;
        private SerializedProperty m_anamorphicFlareSmoothness;
        private SerializedProperty m_anamorphicFlareTint;
        private SerializedProperty m_ghosts;
        private SerializedProperty m_ghostDispersion;
        private SerializedProperty m_ghostRadiusFade;
        private SerializedProperty m_ghostIntensity;
        private SerializedProperty m_halo;
        private SerializedProperty m_separateHaloPass;
        private SerializedProperty m_haloWidth;
        private SerializedProperty m_haloIntensity;
        private SerializedProperty m_haloRadiusFade;
        private SerializedProperty m_chromaticDispersion;

        private SerializedProperty m_downsampleQuality;
        private SerializedProperty m_blurAlgorithm;
        private SerializedProperty m_blurIterations;
        private SerializedProperty m_blurSpread;

        private SerializedProperty m_hdrMode;
        private SerializedProperty m_blendMode;

        private SerializedProperty m_lensColor;
        private SerializedProperty m_anamorphicLensColor;
        private SerializedProperty m_lensOverlay;
        private SerializedProperty m_haloDiffractionOverlay;

        private SerializedProperty m_remapOverlay;
        private SerializedProperty m_overlayMin;
        private SerializedProperty m_overlayMax;

        private Texture2D m_sapphireLogo;


        void OnEnable()
        {
            m_serializedObject = new SerializedObject(target);

            m_downsampleFactor = m_serializedObject.FindProperty("m_downsampleFactor");
            m_thresholdMode = m_serializedObject.FindProperty("m_thresholdMode");
            m_thresholdBias = m_serializedObject.FindProperty("m_thresholdBias");
            m_thresholdMax = m_serializedObject.FindProperty("m_thresholdMax");
            m_thresholdScale = m_serializedObject.FindProperty("m_thresholdScale");
            m_visualizeThreshold = m_serializedObject.FindProperty("m_visualizeThreshold");

            m_flareMode = m_serializedObject.FindProperty("m_flareMode");
            m_anamorphicFlareDirection = m_serializedObject.FindProperty("m_anamorphicFlareDirection");
            m_smoothAnamorphicFlare = m_serializedObject.FindProperty("m_smoothAnamorphicFlare");
            m_anamorphicFlareSmoothness = m_serializedObject.FindProperty("m_anamorphicFlareSmoothness");
            m_anamorphicFlareTint = m_serializedObject.FindProperty("m_anamorphicFlareTint");
            m_ghosts = m_serializedObject.FindProperty("m_ghosts");
            m_ghostDispersion = m_serializedObject.FindProperty("m_ghostDispersion");
            m_ghostRadiusFade = m_serializedObject.FindProperty("m_ghostRadiusFade");
            m_ghostIntensity = m_serializedObject.FindProperty("m_ghostIntensity");
            m_halo = m_serializedObject.FindProperty("m_halo");
            m_separateHaloPass = m_serializedObject.FindProperty("m_separateHaloPass");
            m_haloWidth = m_serializedObject.FindProperty("m_haloWidth");
            m_haloIntensity = m_serializedObject.FindProperty("m_haloIntensity");
            m_haloRadiusFade = m_serializedObject.FindProperty("m_haloRadiusFade");
            m_chromaticDispersion = m_serializedObject.FindProperty("m_chromaticDispersion");

            m_downsampleQuality = m_serializedObject.FindProperty("m_downsampleQuality");
            m_blurAlgorithm = m_serializedObject.FindProperty("m_blurAlgorithm");
            m_blurIterations = m_serializedObject.FindProperty("m_blurIterations");
            m_blurSpread = m_serializedObject.FindProperty("m_blurSpread");

            m_hdrMode = m_serializedObject.FindProperty("m_hdrMode");
            m_blendMode = m_serializedObject.FindProperty("m_blendMode");

            m_lensColor = m_serializedObject.FindProperty("m_lensColor");
            m_anamorphicLensColor = m_serializedObject.FindProperty("m_anamorphicLensColor");
            m_lensOverlay = m_serializedObject.FindProperty("m_lensOverlay");
            m_haloDiffractionOverlay = m_serializedObject.FindProperty("m_haloDiffractionOverlay");

            m_remapOverlay = m_serializedObject.FindProperty("m_remapOverlay");
            m_overlayMin = m_serializedObject.FindProperty("m_overlayMin");
            m_overlayMax = m_serializedObject.FindProperty("m_overlayMax");

            string sapphireLogoPath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(MonoScript.FromScriptableObject(this))) + "/Sapphire_Logo.png";

            m_sapphireLogo = (Texture2D)AssetDatabase.LoadAssetAtPath(sapphireLogoPath, typeof(Texture2D));

            if (m_sapphireLogo != null)
            {
                m_sapphireLogo.hideFlags = HideFlags.HideAndDontSave;
            }
        }


        public override void OnInspectorGUI()
        {
            GUIStyle boldtext = new GUIStyle(GUI.skin.label);
            boldtext.fontStyle = FontStyle.Bold;

            m_serializedObject.Update();

            if (m_sapphireLogo != null)
            {
                Rect logoRect = GUILayoutUtility.GetRect(m_sapphireLogo.width, m_sapphireLogo.height);
                GUI.DrawTexture(logoRect, m_sapphireLogo, ScaleMode.ScaleToFit);
            }

            //EditorGUILayout.Space();

            // General Settings
            EditorGUILayout.LabelField("General Settings", boldtext);
            EditorGUILayout.BeginVertical("Box");

            EditorGUI.indentLevel++;

            m_downsampleFactor.intValue = EditorGUILayout.IntSlider("Downsample Factor", m_downsampleFactor.intValue, 0, 8);
            EditorGUILayout.PropertyField(m_downsampleQuality, new GUIContent("Downsample Quality"));
            EditorGUILayout.PropertyField(m_thresholdMode, new GUIContent("Threshold Mode"));
            m_thresholdBias.floatValue = EditorGUILayout.Slider("Threshold Bias", m_thresholdBias.floatValue, 0.0f, 50.0f);
            m_thresholdMax.floatValue = EditorGUILayout.Slider("Threshold Max", m_thresholdMax.floatValue, 0.0f, 50.0f);
            m_thresholdScale.floatValue = EditorGUILayout.Slider("Threshold Scale", m_thresholdScale.floatValue, 0.0f, 15.0f);
            EditorGUILayout.PropertyField(m_visualizeThreshold, new GUIContent("Visualize Threshold"));

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            // Flare Settings
            EditorGUILayout.LabelField("Flare Settings", boldtext);
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(m_flareMode, new GUIContent("Flare Mode"));

            if (m_flareMode.intValue == 0)
            {
                EditorGUILayout.BeginVertical("TextArea");
                EditorGUILayout.Space();

                m_ghosts.intValue = EditorGUILayout.IntSlider("Ghosts", m_ghosts.intValue, 0, 20);

                if (m_ghosts.intValue > 0)
                {
                    EditorGUILayout.BeginVertical("TextArea");
                    m_ghostDispersion.floatValue = EditorGUILayout.Slider("Ghost Dispersion", m_ghostDispersion.floatValue, 0.0f, 1.5f);
                    m_ghostRadiusFade.floatValue = EditorGUILayout.Slider("Ghost Radius Fade", m_ghostRadiusFade.floatValue, 0.0f, 40.0f);
                    m_ghostIntensity.floatValue = EditorGUILayout.Slider("Ghost Intensity", m_ghostIntensity.floatValue, 0.0f, 5.0f);
                    EditorGUILayout.PropertyField(m_lensColor, new GUIContent("Lens Color Ramp"));
                    //EditorGUILayout.PropertyField(m_apertureDiaphragmShape, new GUIContent("Aperture Diaphragm Shape"));
                    EditorGUILayout.EndVertical();
                }

                EditorGUILayout.Space();

                EditorGUILayout.PropertyField(m_halo, new GUIContent("Halo"));

                if (m_halo.boolValue)
                {
                    EditorGUILayout.BeginVertical("TextArea");
                    EditorGUILayout.PropertyField(m_separateHaloPass, new GUIContent("Separate Halo Pass"));
                    m_haloWidth.floatValue = EditorGUILayout.Slider("Halo Width", m_haloWidth.floatValue, 0.0f, 1.0f);
                    m_haloIntensity.floatValue = EditorGUILayout.Slider("Halo Intensity", m_haloIntensity.floatValue, 0.0f, 10.0f);
                    m_haloRadiusFade.floatValue = EditorGUILayout.Slider("Halo Radius Fade", m_haloRadiusFade.floatValue, 0.0f, 20.0f);
                    EditorGUILayout.PropertyField(m_haloDiffractionOverlay, new GUIContent("Halo Diffraction Overlay"));
                    EditorGUILayout.EndVertical();
                }

                if (m_halo.boolValue || m_ghosts.intValue > 0)
                {
                    EditorGUILayout.Space();

                    m_chromaticDispersion.floatValue = EditorGUILayout.Slider("Chromatic Dispersion", m_chromaticDispersion.floatValue, 0.0f, 20.0f);
                }
                EditorGUILayout.EndVertical();
            }
            else
            {
                EditorGUILayout.BeginVertical("TextArea");
                EditorGUILayout.Space();

                EditorGUILayout.PropertyField(m_anamorphicFlareDirection, new GUIContent("Flare Direction"));

                EditorGUILayout.PropertyField(m_anamorphicFlareTint, new GUIContent("Flare Tint"));

                EditorGUILayout.PropertyField(m_smoothAnamorphicFlare, new GUIContent("Smooth Flares"));

                if (m_smoothAnamorphicFlare.boolValue)
                {
                    EditorGUILayout.BeginVertical("TextArea");
                    EditorGUILayout.Space();
                    m_anamorphicFlareSmoothness.floatValue = EditorGUILayout.Slider("Flare Smoothness", m_anamorphicFlareSmoothness.floatValue, 0.0f, 5.0f);
                    EditorGUILayout.EndVertical();
                }

                if (m_flareMode.intValue == 1)
                {
                    EditorGUILayout.Space();

                    m_ghosts.intValue = EditorGUILayout.IntSlider("Ghosts", m_ghosts.intValue, 0, 20);

                    if (m_ghosts.intValue > 0)
                    {
                        EditorGUILayout.BeginVertical("TextArea");
                        EditorGUILayout.Space();

                        m_ghostDispersion.floatValue = EditorGUILayout.Slider("Ghost Dispersion", m_ghostDispersion.floatValue, 0.0f, 1.5f);
                        m_ghostRadiusFade.floatValue = EditorGUILayout.Slider("Ghost Radius Fade", m_ghostRadiusFade.floatValue, 0.0f, 20.0f);
                        m_ghostIntensity.floatValue = EditorGUILayout.Slider("Ghost Intensity", m_ghostIntensity.floatValue, 0.0f, 5.0f);
                        EditorGUILayout.PropertyField(m_anamorphicLensColor, new GUIContent("Lens Color Ramp"));

                        EditorGUILayout.EndVertical();
                    }
                }

                EditorGUILayout.Space();

                EditorGUILayout.PropertyField(m_halo, new GUIContent("Halo"));

                if (m_halo.boolValue)
                {
                    EditorGUILayout.BeginVertical("TextArea");
                    EditorGUILayout.PropertyField(m_separateHaloPass, new GUIContent("Separate Halo Pass"));
                    m_haloWidth.floatValue = EditorGUILayout.Slider("Halo Width", m_haloWidth.floatValue, 0.0f, 1.0f);
                    m_haloIntensity.floatValue = EditorGUILayout.Slider("Halo Intensity", m_haloIntensity.floatValue, 0.0f, 10.0f);
                    m_haloRadiusFade.floatValue = EditorGUILayout.Slider("Halo Radius Fade", m_haloRadiusFade.floatValue, 0.0f, 20.0f);
                    EditorGUILayout.PropertyField(m_haloDiffractionOverlay, new GUIContent("Halo Diffraction Overlay"));
                    EditorGUILayout.EndVertical();

                    EditorGUILayout.Space();

                    m_chromaticDispersion.floatValue = EditorGUILayout.Slider("Chromatic Dispersion", m_chromaticDispersion.floatValue, 0.0f, 20.0f);
                }

                EditorGUILayout.EndVertical();
            }

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            // Blur Settings
            EditorGUILayout.LabelField("Blur Settings", boldtext);
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(m_blurAlgorithm, new GUIContent("Blur Algorithm"));
            m_blurIterations.intValue = EditorGUILayout.IntSlider("Blur Iterations", m_blurIterations.intValue, 0, 60);
            m_blurSpread.floatValue = EditorGUILayout.Slider("Blur Spread", m_blurSpread.floatValue, 0.0f, 30.0f);

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            // Overlay Settings
            EditorGUILayout.LabelField("Overlay Settings", boldtext);
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(m_lensOverlay, new GUIContent("Lens Dirt Overlay"));
            //EditorGUILayout.PropertyField(m_lensStar, new GUIContent("Lens Starburst Overlay"));

            if (m_lensOverlay.objectReferenceValue != null)
            {
                EditorGUIUtils.BeginSubBlock();
                m_remapOverlay.boolValue = EditorGUILayout.Toggle("Remap Overlay",m_remapOverlay.boolValue);

                if (m_remapOverlay.boolValue)
                {
                    EditorGUIUtils.BeginSubBlock();
                    //float min = m_overlayMin.floatValue;
                    //float max = m_overlayMax.floatValue;
                    //EditorGUILayout.BeginHorizontal();
                    m_overlayMin.floatValue = EditorGUILayout.FloatField("Min", m_overlayMin.floatValue);
                    m_overlayMin.floatValue = Mathf.Clamp(m_overlayMin.floatValue, 0.0f, 10.0f);
                    //EditorGUILayout.MinMaxSlider( ref min, ref max, 0.0f, 10.0f);
                    //m_overlayMin.floatValue = min;
                    //m_overlayMax.floatValue = max;
                    //EditorGUILayout.BeginHorizontal();
                    //EditorGUILayout.FloatField("Min", m_overlayMin.floatValue);
                    m_overlayMax.floatValue = EditorGUILayout.FloatField("Max", m_overlayMax.floatValue);
                    m_overlayMax.floatValue = Mathf.Clamp(m_overlayMax.floatValue, 0.0f, 10.0f);

                    if(m_overlayMin.floatValue > m_overlayMax.floatValue)
                    {
                        m_overlayMin.floatValue = m_overlayMax.floatValue;
                    }
                    //EditorGUILayout.EndHorizontal();
                    EditorGUIUtils.EndSubBlock();
                }
                EditorGUIUtils.EndSubBlock();
            }

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            // Additional Settings
            EditorGUILayout.LabelField("Additional Settings", boldtext);
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(m_hdrMode, new GUIContent("HDR Mode"));
            EditorGUILayout.PropertyField(m_blendMode, new GUIContent("Blend Mode"));

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();

            m_serializedObject.ApplyModifiedProperties();
        }
    }
}
