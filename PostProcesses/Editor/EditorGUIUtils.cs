﻿using UnityEditor;
using UnityEngine;

namespace Sapphire.Editors
{
    public class EditorGUIUtils
    {
        public static void BeginBlock(string a_title)
        {
            GUIStyle boldtext = new GUIStyle(GUI.skin.label);
            boldtext.fontStyle = FontStyle.Bold;
            boldtext.alignment = TextAnchor.MiddleLeft;

            EditorGUILayout.LabelField(a_title, boldtext);
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;
        }

        public static void BeginBlock()
        {
            EditorGUILayout.BeginVertical("Box");
            EditorGUI.indentLevel++;
        }


        public static void EndBlock()
        {
            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }


        public static bool BeginFoldoutBlock(string a_title, bool a_foldState)
        {
            a_foldState = EditorGUILayout.Foldout(a_foldState, a_title);

            if (a_foldState)
            {
                //EditorGUILayout.LabelField(a_title, boldtext);
                EditorGUILayout.BeginVertical("Box");
                EditorGUI.indentLevel++;
            }
            else
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
            }

            return a_foldState;
        }


        public static void EndFoldoutBlock()
        {
            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();
            EditorGUILayout.Space();
            EditorGUILayout.Space();
        }


        public static void BeginSubBlock()
        {
            EditorGUILayout.BeginVertical("TextArea");
            //EditorGUILayout.Space();
        }

        public static void BeginSubBlock(string a_title)
        {
            GUIStyle boldtext = new GUIStyle(GUI.skin.label);
            boldtext.fontStyle = FontStyle.Bold;
            boldtext.alignment = TextAnchor.MiddleLeft;

            EditorGUILayout.LabelField(a_title, boldtext);

            EditorGUILayout.BeginVertical("TextArea");
            //EditorGUILayout.Space();
        }


        public static void EndSubBlock()
        {
            EditorGUILayout.EndVertical();
        }


        public static void BeginElementBlock()
        {
            EditorGUILayout.BeginVertical("Button");
            //EditorGUILayout.Space();
        }


        public static void EndElementBlock()
        {
            EditorGUILayout.EndVertical();
        }
    }
}