﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Sapphire.PostProcess;

namespace Sapphire.Editors
{
    [CustomEditor(typeof(Sapphire.PostProcess.FilmGrain))]
    public class FilmGrainEditor : Editor
    {
        SerializedObject m_serializedObject;

        SerializedProperty m_grainScale;
        SerializedProperty m_colorScale;
        SerializedProperty m_grainSize;
        SerializedProperty m_luminanceScale;
        SerializedProperty m_colored;


        void OnEnable()
        {
            m_serializedObject = new SerializedObject(target);

            m_grainScale = m_serializedObject.FindProperty("m_grainScale");
            m_colorScale = m_serializedObject.FindProperty("m_colorScale");
            m_grainSize = m_serializedObject.FindProperty("m_grainSize");
            m_luminanceScale = m_serializedObject.FindProperty("m_luminanceScale");
            m_colored = m_serializedObject.FindProperty("m_colored");
        }


        public override void OnInspectorGUI()
        {
            GUIStyle boldtext = new GUIStyle(GUI.skin.label);
            boldtext.fontStyle = FontStyle.Bold;

            m_serializedObject.Update();

            EditorGUILayout.Space();

            // Film Grain Settings
            EditorGUILayout.LabelField("General Settings", boldtext);
            EditorGUILayout.BeginVertical("Box");

            EditorGUI.indentLevel++;

            m_grainScale.floatValue = EditorGUILayout.Slider("Grain Scale", m_grainScale.floatValue, 0.0f, 0.1f);
            m_grainSize.floatValue = EditorGUILayout.Slider("Grain Size", m_grainSize.floatValue, 0.25f, 3.0f);
            m_luminanceScale.floatValue = EditorGUILayout.Slider("Luminance Scale", m_luminanceScale.floatValue, 0.0f, 2.0f);

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();
            EditorGUILayout.Space();

            // Color Settings
            EditorGUILayout.LabelField("Color Settings", boldtext);
            EditorGUILayout.BeginVertical("Box");

            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(m_colored, new GUIContent("Colored"));

            if(m_colored.boolValue)
            {
                EditorGUILayout.BeginVertical("TextArea");
                EditorGUILayout.Space();

                m_colorScale.floatValue = EditorGUILayout.Slider("Color Scale", m_colorScale.floatValue, 0.0f, 0.1f);

                EditorGUILayout.EndVertical();
            }

            EditorGUI.indentLevel--;
            EditorGUILayout.EndVertical();

            EditorGUILayout.Space();

            m_serializedObject.ApplyModifiedProperties();
        }
    }
}
