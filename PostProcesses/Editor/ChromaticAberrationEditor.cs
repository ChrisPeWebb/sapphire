﻿using UnityEngine;
using UnityEditor;
using System.Collections;

using Sapphire.PostProcess;

namespace Sapphire.Editors
{
    [CustomEditor(typeof(ChromaticAberration))]
    public class ChromaticAberrationEditor : Editor
    {
        SerializedObject m_serializedObject;

        SerializedProperty m_mode;
        SerializedProperty m_dispersionRadius;
        SerializedProperty m_dispersion;

        SerializedProperty m_vignetting;
        SerializedProperty m_vignetteRadius;
        SerializedProperty m_vignetteDarkness;

        SerializedProperty m_iterations;


        void OnEnable()
        {
            m_serializedObject = new SerializedObject(target);

            m_mode = m_serializedObject.FindProperty("m_mode");
            m_dispersionRadius = m_serializedObject.FindProperty("m_dispersionRadius");
            m_dispersion = m_serializedObject.FindProperty("m_dispersion");

            m_vignetting = m_serializedObject.FindProperty("m_vignetting");
            m_vignetteRadius = m_serializedObject.FindProperty("m_vignetteRadius");
            m_vignetteDarkness = m_serializedObject.FindProperty("m_vignetteDarkness");

            m_iterations = m_serializedObject.FindProperty("m_iterations");
        }


        public override void OnInspectorGUI()
        {
            GUIStyle boldtext = new GUIStyle(GUI.skin.label);
            boldtext.fontStyle = FontStyle.Bold;

            m_serializedObject.Update();

            EditorGUILayout.Space();

            // Chromatic Aberration Settings
            EditorGUIUtils.BeginBlock("Chromatic Aberration Settings");

            EditorGUILayout.PropertyField(m_mode, new GUIContent("Mode"));

            EditorGUIUtils.BeginSubBlock();

            m_iterations.intValue = EditorGUILayout.IntSlider("Iterations", m_iterations.intValue, 0, 32); // 128
            m_dispersionRadius.floatValue = EditorGUILayout.Slider("Dispersion Radius", m_dispersionRadius.floatValue, 0.0f, 20.0f);
            m_dispersion.floatValue = EditorGUILayout.Slider("Dispersion", m_dispersion.floatValue, 0.0f, 100.0f);

            EditorGUIUtils.EndSubBlock();

            EditorGUIUtils.EndBlock();

            // Vignetting Settings
            EditorGUIUtils.BeginBlock("Vignetting Settings");

            EditorGUILayout.PropertyField(m_vignetting, new GUIContent("Vignetting"));

            if (m_vignetting.boolValue)
            {
                EditorGUIUtils.BeginSubBlock();

                m_vignetteRadius.floatValue = EditorGUILayout.Slider("Vignette Radius", m_vignetteRadius.floatValue, 0.0f, 10.0f);
                m_vignetteDarkness.floatValue = EditorGUILayout.Slider("Vignette Darkness", m_vignetteDarkness.floatValue, -1.0f, 0.0f);

                EditorGUIUtils.EndSubBlock();
            }

            EditorGUIUtils.EndSubBlock();

            m_serializedObject.ApplyModifiedProperties();
        }
    }
}
