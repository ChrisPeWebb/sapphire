﻿using System;
using UnityEngine;

namespace Sapphire.PostProcess
{
    public enum HDRMode
    {
        Auto = 0,
        On = 1,
        Off = 2,
    }

    public enum ThresholdMode
    {
        Luminance = 0,
        Intensity = 1,
    }

    public enum FlareMode
    {
        Ghost = 0,
        Anamorphic = 1,
    }

    public enum AnamorphicFlareDirection
    {
        Horizontal = 0,
        Vertical = 1,
    }

    public enum BlendMode
    {
        None = 0,
        Add = 1,
    }

    public enum DownsampleQuality
    {
        Regular = 0,
        High = 1,
    }

    public enum BlurAlgorithm
    {
        //Box = 0,
        SGXOptimizedGaussian = 1,
        MobileGaussian = 2,
        Gaussian = 3,
    }

    [ExecuteInEditMode]
    [AddComponentMenu("Sapphire/PostProcess/LensFlares")]
    [RequireComponent(typeof(Camera))]
    public class LensFlares : MonoBehaviour
    {
        #region Properties
        [SerializeField]
        private int m_downsampleFactor = 2;

        public int DownsampleFactor
        {
            get { return m_downsampleFactor; }
            set { m_downsampleFactor = value; }
        }

        [SerializeField]
        private ThresholdMode m_thresholdMode = ThresholdMode.Luminance;

        public ThresholdMode ThresholdMode
        {
            get { return m_thresholdMode; }
            set { m_thresholdMode = value; }
        }

        [SerializeField]
        private float m_thresholdBias = 1.0f;

        public float ThresholdBias
        {
            get { return m_thresholdBias; }
            set { m_thresholdBias = value; }
        }

        [SerializeField]
        private float m_thresholdMax = 3.5f;

        public float ThresholdMax
        {
            get { return m_thresholdMax; }
            set { m_thresholdMax = value; }
        }

        [SerializeField]
        private float m_thresholdScale = 1.0f;

        public float ThresholdScale
        {
            get { return m_thresholdScale; }
            set { m_thresholdScale = value; }
        }

        [SerializeField]
        private bool m_visualizeThreshold = false;

        public bool VisualizeThreshold
        {
            get { return m_visualizeThreshold; }
            set { m_visualizeThreshold = value; }
        }

        [SerializeField]
        private FlareMode m_flareMode = FlareMode.Ghost;

        public FlareMode FlareMode
        {
            get { return m_flareMode; }
            set { m_flareMode = value; }
        }

        [SerializeField]
        private AnamorphicFlareDirection m_anamorphicFlareDirection = AnamorphicFlareDirection.Horizontal;

        public AnamorphicFlareDirection AnamorphicFlareDirection
        {
            get { return m_anamorphicFlareDirection; }
            set { m_anamorphicFlareDirection = value; }
        }

        [SerializeField]
        private bool m_smoothAnamorphicFlare = true;

        public bool SmoothAnamorphicFlare
        {
            get { return m_smoothAnamorphicFlare; }
            set { m_smoothAnamorphicFlare = value; }
        }

        [SerializeField]
        private float m_anamorphicFlareSmoothness = 1.5f;

        public float AnamorphicFlareSmoothness
        {
            get { return m_anamorphicFlareSmoothness; }
            set { m_anamorphicFlareSmoothness = value; }
        }

        [SerializeField]
        private Color m_anamorphicFlareTint = Color.white;

        public Color AnamorphicFlareTint
        {
            get { return m_anamorphicFlareTint; }
            set { m_anamorphicFlareTint = value; }
        }

        [SerializeField]
        private int m_ghosts = 6;

        public int Ghosts
        {
            get { return m_ghosts; }
            set { m_ghosts = value; }
        }

        [SerializeField]
        private float m_ghostDispersion = 0.2f;

        public float GhostDispersion
        {
            get { return m_ghostDispersion; }
            set { m_ghostDispersion = value; }
        }

        [SerializeField]
        private float m_ghostRadiusFade = 10.0f;

        public float GhostRadiusFade
        {
            get { return m_ghostRadiusFade; }
            set { m_ghostRadiusFade = value; }
        }

        [SerializeField]
        private float m_ghostIntensity = 1.0f;

        public float GhostIntensity
        {
            get { return m_ghostIntensity; }
            set { m_ghostIntensity = value; }
        }

        [SerializeField]
        private bool m_halo = true;

        public bool Halo
        {
            get { return m_halo; }
            set { m_halo = value; }
        }

        [SerializeField]
        private bool m_separateHaloPass = true;

        public bool SeparateHaloPass
        {
            get { return m_separateHaloPass; }
            set { m_separateHaloPass = value; }
        }

        [SerializeField]
        private float m_haloWidth = 0.55f;

        public float HaloWidth
        {
            get { return m_haloWidth; }
            set { m_haloWidth = value; }
        }

        [SerializeField]
        private float m_haloIntensity = 1.0f;

        public float HaloIntensity
        {
            get { return m_haloIntensity; }
            set { m_haloIntensity = value; }
        }

        [SerializeField]
        private float m_haloRadiusFade = 5.0f;

        public float HaloRadiusFade
        {
            get { return m_haloRadiusFade; }
            set { m_haloRadiusFade = value; }
        }

        [SerializeField]
        private float m_chromaticDispersion = 10.0f;

        public float ChromaticDispersion
        {
            get { return m_chromaticDispersion; }
            set { m_chromaticDispersion = value; }
        }

        [SerializeField]
        private DownsampleQuality m_downsampleQuality = DownsampleQuality.High;

        public DownsampleQuality DownsampleQuality
        {
            get { return m_downsampleQuality; }
            set { m_downsampleQuality = value; }
        }

        [SerializeField]
        private BlurAlgorithm m_blurAlgorithm = BlurAlgorithm.Gaussian;

        public BlurAlgorithm BlurAlgorithm
        {
            get { return m_blurAlgorithm; }
            set { m_blurAlgorithm = value; }
        }

        [SerializeField]
        private int m_blurIterations = 2;

        public int BlurIterations
        {
            get { return m_blurIterations; }
            set { m_blurIterations = value; }
        }

        [SerializeField]
        private float m_blurSpread = 0.05f;

        public float BlurSpread
        {
            get { return m_blurSpread; }
            set { m_blurSpread = value; }
        }

        [SerializeField]
        private HDRMode m_hdrMode = HDRMode.Auto;

        public HDRMode HDRMode
        {
            get { return m_hdrMode; }
            set { m_hdrMode = value; }
        }

        [SerializeField]
        private BlendMode m_blendMode = BlendMode.Add;

        public BlendMode BlendMode
        {
            get { return m_blendMode; }
            set { m_blendMode = value; }
        }

        [SerializeField]
        private Texture2D m_lensColor;

        public Texture2D LensColor
        {
            get { return m_lensColor; }
            set { m_lensColor = value; }
        }

        [SerializeField]
        private Texture2D m_anamorphicLensColor;

        public Texture2D AnamorphicLensColor
        {
            get { return m_anamorphicLensColor; }
            set { m_anamorphicLensColor = value; }
        }

        [SerializeField]
        private Texture2D m_lensOverlay;

        public Texture2D LensOverlay
        {
            get { return m_lensOverlay; }
            set { m_lensOverlay = value; }
        }

        [SerializeField]
        private Texture2D m_haloDiffractionOverlay;

        public Texture2D HaloDiffractionOverlay
        {
            get { return m_haloDiffractionOverlay; }
            set { m_haloDiffractionOverlay = value; }
        }

        [SerializeField]
        private bool m_remapOverlay = false;

        public bool RemapOverlay
        {
            get { return m_remapOverlay; }
            set { m_remapOverlay = value; }
        }

        [SerializeField]
        private float m_overlayMin = 0.0f;

        public float OverlayMin
        {
            get { return m_overlayMin; }
            set { m_overlayMin = value; }
        }

        [SerializeField]
        private float m_overlayMax = 1.0f;

        public float OverlayMax
        {
            get { return m_overlayMax; }
            set { m_overlayMax = value; }
        }

        [SerializeField]
        private Shader m_downsampleThresholdShader;
        private Material m_downsampleThresholdMaterial;

        [SerializeField]
        private Shader m_ghostingShader;
        private Material m_ghostingMaterial;

        [SerializeField]
        private Shader m_blurShader;
        private Material m_blurMaterial;

        [SerializeField]
        private Shader m_blendShader;
        private Material m_blendMaterial;
        #endregion

        #region Shader property IDs
        private int m_thresholdBiasID;
        private int m_thresholdMaxID;
        private int m_thresholdScaleID;
        private int m_anamorphicFlareTintID;
        private int m_offsetsID;
        private int m_haloWidthID;
        private int m_haloIntensityID;
        private int m_haloRadiusFadeID;
        private int m_chromaticDispersionID;
        private int m_ghostDispersionID;
        private int m_ghostsID;
        private int m_ghostRadiusFadeID;
        private int m_ghostIntensityID;
        private int m_lensColorID;
        private int m_anamorphicLensColorID;
        private int m_lensOverlayID;
        private int m_overlayMinID;
        private int m_overlayMaxID;
        private int m_haloDiffractionOverlayID;

        private int m_flareTextureID;
        private int m_seperatedHaloTextureID;
        #endregion

        private bool m_doHdr = false;

        private bool m_supportHDRTextures = true;
        private bool m_supportDX11 = false;
        private bool m_isSupported = true;

        private Camera m_camera;

        private RenderTextureUtility m_renderTextureUtility = new RenderTextureUtility();


        public void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if (!m_camera)
                m_camera = GetComponent<Camera>();

            if (CheckResources() == false)
            {
                Graphics.Blit(source, destination);
                return;
            }

            // Ensure we can and want to do HDR
            m_doHdr = false;

            if (m_hdrMode == HDRMode.Auto)
            {
                m_doHdr = source.format == RenderTextureFormat.ARGBHalf && GetComponent<Camera>().hdr;
            }
            else
            {
                m_doHdr = m_hdrMode == HDRMode.On;
            }

            m_doHdr = m_doHdr && m_supportHDRTextures;

            ClampMaterialProperties();

            // Use HDR buffer if we should
            RenderTextureFormat rtFormat = (m_doHdr) ? RenderTextureFormat.ARGBHalf : RenderTextureFormat.Default;

            //
            // Downsample + Threshold pass
            //
            int rtWidth = source.width >> m_downsampleFactor;
            int rtHeight = source.height >> m_downsampleFactor;

            RenderTexture downsampleThresholdRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, rtFormat);
            downsampleThresholdRT.filterMode = FilterMode.Bilinear;

            if(m_thresholdMode == ThresholdMode.Luminance)
            {
                m_downsampleThresholdMaterial.EnableKeyword("THRESHOLD_LUMINANCE");
                m_downsampleThresholdMaterial.DisableKeyword("THRESHOLD_INTENSITY");
            }
            else
            {
                m_downsampleThresholdMaterial.EnableKeyword("THRESHOLD_INTENSITY");
                m_downsampleThresholdMaterial.DisableKeyword("THRESHOLD_LUMINANCE");
            }

            m_downsampleThresholdMaterial.SetFloat(m_thresholdBiasID, m_thresholdBias);
            m_downsampleThresholdMaterial.SetFloat(m_thresholdMaxID, m_thresholdMax);
            m_downsampleThresholdMaterial.SetFloat(m_thresholdScaleID, m_thresholdScale);

            m_downsampleThresholdMaterial.SetColor(m_anamorphicFlareTintID, m_anamorphicFlareTint);

            int downsamplePassOffset = 0;

            // we only want to set tint and do the tint if we are doing an anamorphic style flare
            if(m_flareMode == FlareMode.Anamorphic)
            {
                m_downsampleThresholdMaterial.SetColor(m_anamorphicFlareTintID, m_anamorphicFlareTint);
                downsamplePassOffset = 2;
            }

            if (m_visualizeThreshold)
            {
                Graphics.Blit(source, destination, m_downsampleThresholdMaterial, (int)m_downsampleQuality + downsamplePassOffset);
                m_renderTextureUtility.ReleaseTemporary(downsampleThresholdRT);
                return;
            }

            Graphics.Blit(source, downsampleThresholdRT, m_downsampleThresholdMaterial, (int)m_downsampleQuality + downsamplePassOffset);


            //
            // Blur pass
            //
            RenderTexture blurOutputRT = downsampleThresholdRT;

            float sourceAspect = (1.0f * source.width) / (1.0f * source.height);
            float widthMod = 1.0f / (1.0f * (1 << m_downsampleFactor));
            float oneOverBaseSize = 1.0f / rtWidth;

            switch (m_blurAlgorithm)
            {
                case BlurAlgorithm.SGXOptimizedGaussian:

                    for (int i = 0; i < m_blurIterations; ++i)
                    {
                        float spreadForPass = m_blurSpread * widthMod + (i * 1.0f);

                        if (i == 0 && m_flareMode != FlareMode.Ghost && m_smoothAnamorphicFlare && m_anamorphicFlareDirection == AnamorphicFlareDirection.Horizontal)
                            spreadForPass = m_anamorphicFlareSmoothness * widthMod + (i * 1.0f);

                        m_blurMaterial.SetVector(m_offsetsID, new Vector4(spreadForPass, 0.0f, 0.0f, 0.0f));

                        RenderTexture tempBlurRT;

                        if (m_flareMode == FlareMode.Ghost || m_anamorphicFlareDirection == AnamorphicFlareDirection.Vertical || (i == 0 && m_smoothAnamorphicFlare))
                        {
                            // vertical blur
                            tempBlurRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, source.format);
                            tempBlurRT.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(blurOutputRT, tempBlurRT, m_blurMaterial, 2);
                            m_renderTextureUtility.ReleaseTemporary(blurOutputRT);
                            blurOutputRT = tempBlurRT;
                        }

                        if (i == 0 && m_flareMode != FlareMode.Ghost && m_smoothAnamorphicFlare)
                        {
                            if (m_anamorphicFlareDirection == AnamorphicFlareDirection.Vertical)
                            {
                                spreadForPass = m_anamorphicFlareSmoothness * widthMod + (i * 1.0f);
                            }
                            else
                            {
                                spreadForPass = m_blurSpread * widthMod + (i * 1.0f);
                            }

                            m_blurMaterial.SetVector(m_offsetsID, new Vector4(spreadForPass, 0.0f, 0.0f, 0.0f));
                        }


                        if (m_flareMode == FlareMode.Ghost || m_anamorphicFlareDirection == AnamorphicFlareDirection.Horizontal || (i == 0 && m_smoothAnamorphicFlare))
                        {
                            // horizontal blur
                            tempBlurRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, source.format);
                            tempBlurRT.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(blurOutputRT, tempBlurRT, m_blurMaterial, 3);
                            m_renderTextureUtility.ReleaseTemporary(blurOutputRT);
                            blurOutputRT = tempBlurRT;
                        }
                    }

                    break;

                case BlurAlgorithm.MobileGaussian:

                    for (int i = 0; i < m_blurIterations; ++i)
                    {
                        float spreadForPass = m_blurSpread * widthMod + (i * 1.0f);

                        if (i == 0 && m_flareMode != FlareMode.Ghost && m_smoothAnamorphicFlare && m_anamorphicFlareDirection == AnamorphicFlareDirection.Horizontal)
                            spreadForPass = m_anamorphicFlareSmoothness * widthMod + (i * 1.0f);

                        m_blurMaterial.SetVector(m_offsetsID, new Vector4(spreadForPass, 0.0f, 0.0f, 0.0f));

                        RenderTexture tempBlurRT;

                        if (m_flareMode == FlareMode.Ghost || m_anamorphicFlareDirection == AnamorphicFlareDirection.Vertical || (i == 0 && m_smoothAnamorphicFlare))
                        {
                            // vertical blur
                            tempBlurRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, source.format);
                            tempBlurRT.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(blurOutputRT, tempBlurRT, m_blurMaterial, 4);
                            m_renderTextureUtility.ReleaseTemporary(blurOutputRT);
                            blurOutputRT = tempBlurRT;
                        }

                        if (i == 0 && m_flareMode != FlareMode.Ghost && m_smoothAnamorphicFlare)
                        {
                            if (m_anamorphicFlareDirection == AnamorphicFlareDirection.Vertical)
                            {
                                spreadForPass = m_anamorphicFlareSmoothness * widthMod + (i * 1.0f);
                            }
                            else
                            {
                                spreadForPass = m_blurSpread * widthMod + (i * 1.0f);
                            }

                            m_blurMaterial.SetVector(m_offsetsID, new Vector4(spreadForPass, 0.0f, 0.0f, 0.0f));
                        }


                        if (m_flareMode == FlareMode.Ghost || m_anamorphicFlareDirection == AnamorphicFlareDirection.Horizontal || (i == 0 && m_smoothAnamorphicFlare))
                        {
                            // horizontal blur
                            tempBlurRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, source.format);
                            tempBlurRT.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(blurOutputRT, tempBlurRT, m_blurMaterial, 5);
                            m_renderTextureUtility.ReleaseTemporary(blurOutputRT);
                            blurOutputRT = tempBlurRT;
                        }
                    }

                    break;

                case BlurAlgorithm.Gaussian:

                    for (int i = 0; i < m_blurIterations; ++i)
                    {
                        float spreadForPass = (1.0f + (i * 0.5f)) * m_blurSpread;

                        if (i == 0 && m_flareMode != FlareMode.Ghost && m_smoothAnamorphicFlare && m_anamorphicFlareDirection == AnamorphicFlareDirection.Horizontal)
                            spreadForPass = (1.0f + (i * 0.5f)) * m_anamorphicFlareSmoothness;

                        RenderTexture tempBlurRT;

                        if (m_flareMode == FlareMode.Ghost || m_anamorphicFlareDirection == AnamorphicFlareDirection.Vertical || (i == 0 && m_smoothAnamorphicFlare))
                        {
                            // vertical blur
                            m_blurMaterial.SetVector(m_offsetsID, new Vector4(0.0f, spreadForPass * oneOverBaseSize, 0.0f, 0.0f));

                            tempBlurRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, source.format);
                            tempBlurRT.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(blurOutputRT, tempBlurRT, m_blurMaterial, 6);
                            m_renderTextureUtility.ReleaseTemporary(blurOutputRT);
                            blurOutputRT = tempBlurRT;
                        }

                        if (i == 0 && m_flareMode != FlareMode.Ghost && m_smoothAnamorphicFlare)
                        {
                            if (m_anamorphicFlareDirection == AnamorphicFlareDirection.Vertical)
                            {
                                spreadForPass = (1.0f + (i * 0.5f)) * m_anamorphicFlareSmoothness;
                            }
                            else
                            {
                                spreadForPass = (1.0f + (i * 0.5f)) * m_blurSpread;
                            }
                        }

                        if (m_flareMode == FlareMode.Ghost || m_anamorphicFlareDirection == AnamorphicFlareDirection.Horizontal || (i == 0 && m_smoothAnamorphicFlare))
                        {
                            // horizontal blur
                            m_blurMaterial.SetVector(m_offsetsID, new Vector4((spreadForPass / sourceAspect) * oneOverBaseSize, 0.0f, 0.0f, 0.0f));

                            tempBlurRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, source.format);
                            tempBlurRT.filterMode = FilterMode.Bilinear;
                            Graphics.Blit(blurOutputRT, tempBlurRT, m_blurMaterial, 6);
                            m_renderTextureUtility.ReleaseTemporary(blurOutputRT);
                            blurOutputRT = tempBlurRT;
                        }
                    }

                    break;
            }


            //
            // Ghosting pass
            //

            // Ghost RT will be used for regular and combined ghost, as they are mutually exclusive
            RenderTexture ghostingRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, rtFormat);
            ghostingRT.filterMode = FilterMode.Bilinear;

            RenderTexture haloRT = m_renderTextureUtility.GetTemporary(rtWidth, rtHeight, 0, rtFormat);
            haloRT.filterMode = FilterMode.Bilinear;

            // Do seperated halo pass first. This can be done with any flare mode
            if (m_halo && m_separateHaloPass)
            {
                m_ghostingMaterial.DisableKeyword("LENS_RAMP_ON");
                m_ghostingMaterial.DisableKeyword("ANAMORPHIC_ON");
                m_ghostingMaterial.DisableKeyword("GHOST_ON");
                m_ghostingMaterial.EnableKeyword("HALO_ON");

                m_ghostingMaterial.SetFloat(m_haloWidthID, m_haloWidth);
                m_ghostingMaterial.SetFloat(m_haloIntensityID, m_haloIntensity);
                m_ghostingMaterial.SetFloat(m_haloRadiusFadeID, m_haloRadiusFade);
                m_ghostingMaterial.SetFloat(m_chromaticDispersionID, m_chromaticDispersion);

                // TODO: It is not great that this is required
                m_ghostingMaterial.SetFloat(m_ghostDispersionID, m_ghostDispersion);

                Graphics.Blit(blurOutputRT, haloRT, m_ghostingMaterial, 0);
            }
            else if (m_halo)
            {
                // Halo will be done as part of combined ghost and halo pass
                m_ghostingMaterial.EnableKeyword("HALO_ON");

                m_ghostingMaterial.SetFloat(m_haloWidthID, m_haloWidth);
                m_ghostingMaterial.SetFloat(m_haloIntensityID, m_haloIntensity);
                m_ghostingMaterial.SetFloat(m_haloRadiusFadeID, m_haloRadiusFade);
            }
            else
            {
                m_ghostingMaterial.DisableKeyword("HALO_ON");
            }

            m_ghostingMaterial.SetFloat(m_ghostDispersionID, m_ghostDispersion);
            m_ghostingMaterial.SetFloat(m_chromaticDispersionID, m_chromaticDispersion);

            if (m_ghosts != 0)
            {
                m_ghostingMaterial.EnableKeyword("GHOST_ON");

                m_ghostingMaterial.SetFloat(m_ghostsID, m_ghosts);
                m_ghostingMaterial.SetFloat(m_ghostRadiusFadeID, m_ghostRadiusFade);
                m_ghostingMaterial.SetFloat(m_ghostIntensityID, m_ghostIntensity);
            }
            else
            {
                m_ghostingMaterial.DisableKeyword("GHOST_ON");
            }

            if (m_flareMode == FlareMode.Ghost)
            {
                m_ghostingMaterial.DisableKeyword("ANAMORPHIC_ON");

                if (m_lensColor != null)
                {
                    m_ghostingMaterial.EnableKeyword("LENS_RAMP_ON");

                    m_ghostingMaterial.SetTexture(m_lensColorID, m_lensColor);
                }
                else
                {
                    m_ghostingMaterial.DisableKeyword("LENS_RAMP_ON");
                }

                // If we are doing a ghost, or non seperated halo, blit it
                if(m_ghosts != 0 || (m_halo && !m_separateHaloPass))
                {
                    Graphics.Blit(blurOutputRT, ghostingRT, m_ghostingMaterial, 0);
                }
            }

            //
            // Anamorphic Ghosting pass
            //

            else if (m_flareMode == FlareMode.Anamorphic)
            {
                m_ghostingMaterial.EnableKeyword("ANAMORPHIC_ON");

                if (m_anamorphicLensColor != null)
                {
                    m_ghostingMaterial.EnableKeyword("LENS_RAMP_ON");

                    m_ghostingMaterial.SetTexture(m_anamorphicLensColorID, m_anamorphicLensColor);
                }
                else
                {
                    m_ghostingMaterial.DisableKeyword("LENS_RAMP_ON");
                }

                // If we are doing a ghost, or non seperated halo, blit it
                if (m_ghosts != 0 || (m_halo && !m_separateHaloPass))
                {
                    Graphics.Blit(blurOutputRT, ghostingRT, m_ghostingMaterial, 0);
                }
            }


            //
            // Blend pass   (This should be done after any other post processes. Currently not done.
            //
            switch (m_blendMode)
            {
                case BlendMode.Add:
                    m_blendMaterial.DisableKeyword("BLENDING_NONE");
                    m_blendMaterial.EnableKeyword("BLENDING_ADDITIVE");
                    break;

                case BlendMode.None:
                    m_blendMaterial.DisableKeyword("BLENDING_ADDITIVE");
                    m_blendMaterial.EnableKeyword("BLENDING_NONE");
                    break;
            }

            if (m_lensOverlay)
            {
                m_blendMaterial.SetTexture(m_lensOverlayID, m_lensOverlay);
                m_blendMaterial.EnableKeyword("OVERLAY_ON");

                if(m_remapOverlay)
                {
                    m_blendMaterial.EnableKeyword("REMAP_OVERLAY_ON");

                    m_blendMaterial.SetFloat(m_overlayMinID, m_overlayMin);
                    m_blendMaterial.SetFloat(m_overlayMaxID, m_overlayMax);
                }
                else
                {
                    m_blendMaterial.DisableKeyword("REMAP_OVERLAY_ON");
                }
            }
            else
            {
                m_blendMaterial.DisableKeyword("OVERLAY_ON");
            }

            if (m_haloDiffractionOverlay && m_halo)
            {
                m_blendMaterial.SetTexture(m_haloDiffractionOverlayID, m_haloDiffractionOverlay);

                m_blendMaterial.EnableKeyword("HALO_DIFFRACTION_ON");
            }
            else
            {
                m_blendMaterial.DisableKeyword("HALO_DIFFRACTION_ON");
            }

            if (!m_halo || !m_separateHaloPass)
            {
                m_blendMaterial.DisableKeyword("SEPERATED_HALO_ON");
                m_blendMaterial.EnableKeyword("FLARE_ON");

                if (m_flareMode == FlareMode.Anamorphic && !( m_ghosts != 0 || (m_halo && !m_separateHaloPass)))
                {
                    m_blendMaterial.SetTexture(m_flareTextureID, blurOutputRT);
                }
                else
                {
                    m_blendMaterial.SetTexture(m_flareTextureID, ghostingRT);
                }

                Graphics.Blit(source, destination, m_blendMaterial, 0);
            }
            // If doing a separated halo, we need to blend slightly differently
            else
            {
                m_blendMaterial.EnableKeyword("SEPERATED_HALO_ON");

                // Even if there are no ghosts, ghostingRT must be blended in for anamorphic mode (as that is where the anamorphoc flare is)
                if (m_flareMode == FlareMode.Ghost && m_ghosts == 0)
                {
                    m_blendMaterial.DisableKeyword("FLARE_ON");

                    // Halo blend
                    m_blendMaterial.SetTexture(m_seperatedHaloTextureID, haloRT);

                    Graphics.Blit(source, destination, m_blendMaterial, 0);
                }
                else
                {
                    m_blendMaterial.EnableKeyword("FLARE_ON");

                    // Ghost blend
                    if (m_flareMode == FlareMode.Anamorphic && !(m_ghosts != 0 || (m_halo && !m_separateHaloPass)))
                    {
                        m_blendMaterial.SetTexture(m_flareTextureID, blurOutputRT);
                    }
                    else
                    {
                        m_blendMaterial.SetTexture(m_flareTextureID, ghostingRT);
                    }

                    // Halo blend
                    m_blendMaterial.SetTexture(m_seperatedHaloTextureID, haloRT);

                    Graphics.Blit(source, destination, m_blendMaterial, 0);
                }
            }

            m_renderTextureUtility.ReleaseTemporary(blurOutputRT); // Clears up downsampleThresholdRT also
            m_renderTextureUtility.ReleaseTemporary(haloRT);
            m_renderTextureUtility.ReleaseTemporary(ghostingRT);

            m_renderTextureUtility.ReleaseAllTemporary();
        }


        private void ClampMaterialProperties()
        {
            m_thresholdBias = Mathf.Clamp(m_thresholdBias, 0.0f, 50.0f);
            m_thresholdScale = Mathf.Clamp(m_thresholdScale, 0.0f, 15.0f);

            m_downsampleFactor = Mathf.Clamp(m_downsampleFactor, 0, 8);

            m_ghosts = Mathf.Clamp(m_ghosts, 0, 20);
            m_ghostDispersion = Mathf.Clamp(m_ghostDispersion, 0.0f, 1.5f);
            m_ghostRadiusFade = Mathf.Clamp(m_ghostRadiusFade, 0.0f, 40.0f);
            m_ghostIntensity = Mathf.Clamp(m_ghostIntensity, 0.0f, 5.0f);
            m_haloWidth = Mathf.Clamp(m_haloWidth, 0.0f, 1.0f);
            m_haloRadiusFade = Mathf.Clamp(m_haloRadiusFade, 0.0f, 20.0f);
            m_chromaticDispersion = Mathf.Clamp(m_chromaticDispersion, 0.0f, 20.0f);
            m_anamorphicFlareSmoothness = Mathf.Clamp(m_anamorphicFlareSmoothness, 0.0f, 5.0f);

            m_blurIterations = Mathf.Clamp(m_blurIterations, 0, 60);
            m_blurSpread = Mathf.Clamp(m_blurSpread, 0.0f, 30.0f);

            m_overlayMin = Mathf.Clamp(m_overlayMin, 0.0f, 10.0f);
            m_overlayMax = Mathf.Clamp(m_overlayMax, 0.0f, 10.0f);

            if(m_overlayMin > m_overlayMax)
            {
                m_overlayMin = m_overlayMax;
            }
        }


        private bool CheckResources()
        {
            CheckSupport(false);

            GetMaterialIDs();

            m_downsampleThresholdMaterial = CheckShaderAndCreateMaterial(m_downsampleThresholdShader, m_downsampleThresholdMaterial);
            m_ghostingMaterial = CheckShaderAndCreateMaterial(m_ghostingShader, m_ghostingMaterial);
            m_blurMaterial = CheckShaderAndCreateMaterial(m_blurShader, m_blurMaterial);
            m_blendMaterial = CheckShaderAndCreateMaterial(m_blendShader, m_blendMaterial);

            if (!m_isSupported)
                ReportAutoDisable();

            return m_isSupported;
        }


        private Material CheckShaderAndCreateMaterial(Shader s, Material m2Create)
        {
            if (!s)
            {
                Debug.LogError("Missing shader in " + ToString());
                enabled = false;
                return null;
            }

            if (s.isSupported && m2Create != null && m2Create.shader == s)
                return m2Create;

            if (!s.isSupported)
            {
                NotSupported();
                Debug.LogError("The shader " + s.ToString() + " on effect " + ToString() + " is not supported on this platform!");
                return null;
            }
            else
            {
                m2Create = new Material(s);
                m2Create.hideFlags = HideFlags.DontSave;
                if (m2Create)
                    return m2Create;
                else return null;
            }
        }


        private Material CreateMaterial(Shader s, Material m2Create)
        {
            if (!s)
            {
                Debug.Log("Missing shader in " + ToString());
                return null;
            }

            if (m2Create && (m2Create.shader == s) && (s.isSupported))
                return m2Create;

            if (!s.isSupported)
            {
                Debug.LogError("Shader is not supported: " + s.ToString());
                return null;
            }
            else
            {
                m2Create = new Material(s);
                m2Create.hideFlags = HideFlags.DontSave;
                if (m2Create)
                    return m2Create;
                else
                {
                    Debug.LogError("Could not create material with shader: " + s.ToString());
                    return null;
                }
            }
        }


        private void OnEnable()
        {
            m_isSupported = true;
        }


        private bool CheckSupport()
        {
            return CheckSupport(false);
        }


        private void Start()
        {
            CheckResources();
        }


        private bool CheckSupport(bool needDepth)
        {
            m_isSupported = true;
            m_supportHDRTextures = SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf);
            m_supportDX11 = SystemInfo.graphicsShaderLevel >= 50 && SystemInfo.supportsComputeShaders;

            if (!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures)
            {
                NotSupported();
                return false;
            }

            if (needDepth && !SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.Depth))
            {
                NotSupported();
                return false;
            }

            if (needDepth)
                GetComponent<Camera>().depthTextureMode |= DepthTextureMode.Depth;

            return true;
        }


        private bool CheckSupport(bool needDepth, bool needHdr)
        {
            if (!CheckSupport(needDepth))
                return false;

            if (needHdr && !m_supportHDRTextures)
            {
                NotSupported();
                return false;
            }

            return true;
        }


        private bool Dx11Support()
        {
            return m_supportDX11;
        }


        private void ReportAutoDisable()
        {
            Debug.LogWarning("The image effect " + ToString() + " has been disabled as it's not supported on the current platform.");
        }


        private void NotSupported()
        {
            enabled = false;
            m_isSupported = false;
            return;
        }


        private void GetMaterialIDs()
        {
            m_thresholdBiasID = Shader.PropertyToID("iBias");
            m_thresholdMaxID = Shader.PropertyToID("iMax");
            m_thresholdScaleID = Shader.PropertyToID("iScale");
            m_anamorphicFlareTintID = Shader.PropertyToID("iTint");
            m_offsetsID = Shader.PropertyToID("iOffsets");
            m_haloWidthID = Shader.PropertyToID("iHaloWidth");
            m_haloIntensityID = Shader.PropertyToID("iHaloIntensity");
            m_haloRadiusFadeID = Shader.PropertyToID("iHaloRadiusFade");
            m_chromaticDispersionID = Shader.PropertyToID("iChromaticDispersion");
            m_ghostDispersionID = Shader.PropertyToID("iGhostDispersion");
            m_ghostsID = Shader.PropertyToID("iGhosts");
            m_ghostRadiusFadeID = Shader.PropertyToID("iGhostRadiusFade");
            m_ghostIntensityID = Shader.PropertyToID("iGhostIntensity");
            m_lensColorID = Shader.PropertyToID("iLensColor");
            m_anamorphicLensColorID = Shader.PropertyToID("iAnamorphicLensColor");
            m_lensOverlayID = Shader.PropertyToID("iLensOverlayTex");
            m_overlayMinID = Shader.PropertyToID("iOverlayMin");
            m_overlayMaxID = Shader.PropertyToID("iOverlayMax");
            m_haloDiffractionOverlayID = Shader.PropertyToID("iHaloDiffractionOverlay");

            m_flareTextureID = Shader.PropertyToID("iFlareTex");
            m_seperatedHaloTextureID = Shader.PropertyToID("iSeperatedHaloTex");
        }


        private void DrawBorder(RenderTexture dest, Material material)
        {
            float x1;
            float x2;
            float y1;
            float y2;

            RenderTexture.active = dest;
            bool invertY = true; // source.texelSize.y < 0.0ff;
            // Set up the simple Matrix
            GL.PushMatrix();
            GL.LoadOrtho();

            for (int i = 0; i < material.passCount; i++)
            {
                material.SetPass(i);

                float y1_; float y2_;
                if (invertY)
                {
                    y1_ = 1.0f; y2_ = 0.0f;
                }
                else
                {
                    y1_ = 0.0f; y2_ = 1.0f;
                }

                // left
                x1 = 0.0f;
                x2 = 0.0f + 1.0f / (dest.width * 1.0f);
                y1 = 0.0f;
                y2 = 1.0f;
                GL.Begin(GL.QUADS);

                GL.TexCoord2(0.0f, y1_); GL.Vertex3(x1, y1, 0.1f);
                GL.TexCoord2(1.0f, y1_); GL.Vertex3(x2, y1, 0.1f);
                GL.TexCoord2(1.0f, y2_); GL.Vertex3(x2, y2, 0.1f);
                GL.TexCoord2(0.0f, y2_); GL.Vertex3(x1, y2, 0.1f);

                // right
                x1 = 1.0f - 1.0f / (dest.width * 1.0f);
                x2 = 1.0f;
                y1 = 0.0f;
                y2 = 1.0f;

                GL.TexCoord2(0.0f, y1_); GL.Vertex3(x1, y1, 0.1f);
                GL.TexCoord2(1.0f, y1_); GL.Vertex3(x2, y1, 0.1f);
                GL.TexCoord2(1.0f, y2_); GL.Vertex3(x2, y2, 0.1f);
                GL.TexCoord2(0.0f, y2_); GL.Vertex3(x1, y2, 0.1f);

                // top
                x1 = 0.0f;
                x2 = 1.0f;
                y1 = 0.0f;
                y2 = 0.0f + 1.0f / (dest.height * 1.0f);

                GL.TexCoord2(0.0f, y1_); GL.Vertex3(x1, y1, 0.1f);
                GL.TexCoord2(1.0f, y1_); GL.Vertex3(x2, y1, 0.1f);
                GL.TexCoord2(1.0f, y2_); GL.Vertex3(x2, y2, 0.1f);
                GL.TexCoord2(0.0f, y2_); GL.Vertex3(x1, y2, 0.1f);

                // bottom
                x1 = 0.0f;
                x2 = 1.0f;
                y1 = 1.0f - 1.0f / (dest.height * 1.0f);
                y2 = 1.0f;

                GL.TexCoord2(0.0f, y1_); GL.Vertex3(x1, y1, 0.1f);
                GL.TexCoord2(1.0f, y1_); GL.Vertex3(x2, y1, 0.1f);
                GL.TexCoord2(1.0f, y2_); GL.Vertex3(x2, y2, 0.1f);
                GL.TexCoord2(0.0f, y2_); GL.Vertex3(x1, y2, 0.1f);

                GL.End();
            }

            GL.PopMatrix();
        }
    }
}
