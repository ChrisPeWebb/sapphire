﻿using UnityEngine;

namespace Sapphire.PostProcess
{
    public enum AberrationMode
    {
        FullSpectrum = 0,
        AchromaticDoublet = 1,
    }

    [ExecuteInEditMode]
    [AddComponentMenu("Sapphire/PostProcess/ChromaticAberration")]
    [RequireComponent(typeof(Camera))]
    public class ChromaticAberration : MonoBehaviour
    {
        [SerializeField]
        private AberrationMode m_mode = AberrationMode.FullSpectrum;

        public AberrationMode Mode
        {
            get { return m_mode; }
            set { m_mode = value; }
        }

        [SerializeField]
        private float m_dispersionRadius = 5.0f;
        public float DispersionRadius
        {
            get { return m_dispersionRadius; }
            set { m_dispersionRadius = value; }
        }

        [SerializeField]
        private float m_dispersion = 0.15f;
        public float Dispersion
        {
            get { return m_dispersion; }
            set { m_dispersion = value; }
        }

        [SerializeField]
        private bool m_vignetting = true;
        public bool Vignetting
        {
            get { return m_vignetting; }
            set { m_vignetting = value; }
        }

        [SerializeField]
        private float m_vignetteRadius = 5.0f;
        public float VignetteRadius
        {
            get { return m_vignetteRadius; }
            set { m_vignetteRadius = value; }
        }

        [SerializeField]
        private float m_vignetteDarkness = 0.005f;
        public float VignetteDarkness
        {
            get { return m_vignetteDarkness; }
            set { m_vignetteDarkness = value; }
        }

        [SerializeField]
        private int m_iterations = 8;
        public int Iterations
        {
            get { return m_iterations; }
            set { m_iterations = value; }
        }

        [SerializeField]
        private Shader m_shader;

        private Material m_material;

        private bool m_isSupported = true;


        private void OnDisable()
        {
            if (m_material)
                DestroyImmediate(m_material);
        }


        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if (CheckResources() == false)
            {
                Graphics.Blit(source, destination);
                return;
            }

            ClampMaterialProperties();

            m_material.SetFloat("iDispersion", m_dispersion);
            m_material.SetFloat("iDispersionRadius", m_dispersionRadius);
            m_material.SetInt("iIterations", m_iterations);

            switch(m_mode)
            {
                case AberrationMode.FullSpectrum:
                    m_material.EnableKeyword("FULL_ABERRATION_ON");
                    break;

                case AberrationMode.AchromaticDoublet:
                    m_material.DisableKeyword("FULL_ABERRATION_ON");
                    break;
            }

            if (m_vignetting)
            {
                m_material.SetFloat("iVignetteDarkness", m_vignetteDarkness);
                m_material.SetFloat("iVignetteRadius", m_vignetteRadius);
                m_material.EnableKeyword("VIGNETTING_ON");
            }
            else
            {
                m_material.DisableKeyword("VIGNETTING_ON");
            }

            Graphics.Blit(source, destination, m_material, 0);
        }


        private void ClampMaterialProperties()
        {
            if (m_dispersion < 0)
                m_dispersion = 0;

            m_iterations = Mathf.Clamp(m_iterations, 3, 128);

            m_vignetteDarkness = Mathf.Clamp(m_vignetteDarkness, -2.0f, 0.0f);
            m_vignetteRadius = Mathf.Clamp(m_vignetteRadius, 0.0f, 10.0f);
            m_dispersionRadius = Mathf.Clamp(m_dispersionRadius, 0.0f, 20.0f);
        }


        private bool CheckResources()
        {
            CheckSupport();

            m_material = CheckShaderAndCreateMaterial(m_shader, m_material);

            if (!m_isSupported)
                ReportAutoDisable();

            return m_isSupported;
        }


        private Material CheckShaderAndCreateMaterial(Shader s, Material m2Create)
        {
            if (!s)
            {
                Debug.Log("Missing shader in " + ToString());
                enabled = false;
                return null;
            }

            if (s.isSupported && m2Create && m2Create.shader == s)
                return m2Create;

            if (!s.isSupported)
            {
                NotSupported();
                Debug.Log("The shader " + s.ToString() + " on effect " + ToString() + " is not supported on this platform!");
                return null;
            }
            else
            {
                m2Create = new Material(s);
                m2Create.hideFlags = HideFlags.DontSave;
                if (m2Create)
                    return m2Create;
                else return null;
            }
        }


        private void Start()
        {
            CheckResources();
        }

        private void OnEnable()
        {
            m_isSupported = true;
        }


        private bool CheckSupport()
        {
            m_isSupported = true;

            if (!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures)
            {
                NotSupported();
                return false;
            }

            return true;
        }


        private void NotSupported()
        {
            enabled = false;
            m_isSupported = false;
            return;
        }


        private void ReportAutoDisable()
        {
            Debug.LogWarning("The image effect " + ToString() + " has been disabled as it's not supported on the current platform.");
        }

    }
}