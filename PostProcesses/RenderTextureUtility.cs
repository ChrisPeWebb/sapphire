﻿using UnityEngine;
using System.Collections.Generic;

namespace Sapphire.PostProcess
{
    public class RenderTextureUtility
    {
        // If this limit is exceeded, something has probably gone wrong and we are leaking.
        private const int k_maxTempRTLimit = 30;

        private List<RenderTexture> m_temporaryRTs = new List<RenderTexture>();

        public RenderTexture GetTemporary(int width, int height, int depthBuffer = 0, RenderTextureFormat format = RenderTextureFormat.ARGBHalf, FilterMode filterMode = FilterMode.Bilinear)
        {
            var rt = RenderTexture.GetTemporary(width, height, depthBuffer, format);
            rt.filterMode = filterMode;
            rt.wrapMode = TextureWrapMode.Clamp;
            rt.name = "SapphireTempTexture";
            m_temporaryRTs.Add(rt);

            if(m_temporaryRTs.Count > k_maxTempRTLimit)
            {
                Debug.LogError("Maximum render target count exceeded. Something has probably gone wrong and we are leaking render targets. Releasing all temporary render targets.");
                ReleaseAllTemporary();
            }

            return rt;
        }


        public void ReleaseTemporary(RenderTexture rt)
        {
            if (rt == null)
                return;

            if (!m_temporaryRTs.Contains(rt))
            {
                Debug.LogErrorFormat("Attempting to remove texture that was not allocated: {0}", rt);
                return;
            }

            m_temporaryRTs.Remove(rt);
            RenderTexture.ReleaseTemporary(rt);
        }


        public void ReleaseAllTemporary()
        {
            for (int i = 0; i < m_temporaryRTs.Count; ++i)
            {
                RenderTexture.ReleaseTemporary(m_temporaryRTs[i]);
            }

            m_temporaryRTs.Clear();
        }
    }
}
